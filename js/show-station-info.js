(function ($) {
  Drupal.behaviors.showStationInfoBehavior = {
    attach: function (context, settings) {
      $(context).find('#stationInfoDialog').once('show-station-info').each(function() {
        initStationsInfoDialog($);
        loadStationsInfo($);
        ymaps.ready(function() {
            myMap = new ymaps.Map('stationMap', {
                center: [0, 0], 
                zoom: 17,
                controls: ['zoomControl']
            });
            cityMarker = new ymaps.Placemark([0, 0]);
            myMap.geoObjects.add(cityMarker);
        });
        handleStationInfoButtons($);
      });
    }
  };
})(jQuery);

var myMap, cityMarker;
var stationsInfoList = { };

function loadStationsInfo($) {
    $('.btn-station-additional-info').each(function() {
        var stationId = $(this).data('station-id');
        if (!stationsInfoList[stationId]) {
            stationsInfoList[stationId] = [];
        }
    });

    for (id in stationsInfoList) {
        $.ajax('/api/station/' + id, {
            context: {
                id: id
            }
        }).done(function(data) {
            stationsInfoList[this.id] = data;
            showButtonsIfNeeded($, this.id);
        });
    }
}

function showButtonsIfNeeded($, stationId) {
    if (stationsInfoList[stationId].length !== 0) {
        $('[data-station-id="' + stationId + '"]').show();
    }
}

function initStationsInfoDialog($) {
    $('#stationInfoDialog').dialog({
        autoOpen: false,
        modal: true,
        draggable: false,
        height: 600,
        width: 1250,
        resizable: false,
        clickOut: true,
        responsive: true,
        dialogClass: 'my-dialog',
        scaleH: 0.95,
        scaleW: 0.95
    });

    $('.station-info-toggle-btn').click(function() {
        var firstColumnId = $('#stationInfoDialog .col-sm-6').first().attr('id');
        if (firstColumnId === 'mapContainer') {
            $(this).text('Показать карту');
            $('#mapContainer').before($('#descriptionContainer'));
        } else {
            $(this).text('Показать описание');
            $('#descriptionContainer').before($('#mapContainer'));
        }
    });
}

function handleStationInfoButtons($) {
    $('.btn-station-additional-info').each(function() {
        $(this).click(function() {
            var stationId = $(this).data('station-id');
            var info = stationsInfoList[stationId];
            
            if (info.latitude) {
                var coords = [
                    info.latitude,
                    info.longitude
                ];
                cityMarker.geometry.setCoordinates(coords);
                myMap.setCenter(coords);
                myMap.setZoom(17);
                $('#mapContainer').show();
            } else {
                $('#mapContainer').hide();
            }

            if (info.photo) {
                $('.station-photo').attr('src', info.photo);
                $('.station-photo').show();
            } else {
                $('.station-photo').hide();
            }

            if (info.description) {
                $('.station-description').html(info.description);
            } else {
                $('.station-description').empty();
            }

            if (!info.photo && !info.description) {
                $('#descriptionContainer').hide();
            } else {
                $('#descriptionContainer').show();
            }

            if ((!info.photo && !info.description) || !info.latitude) {
                $('.station-info-toggle-btn').hide();
                $('.station-info-toggle-btn').removeClass('visible-xs');
            } else {
                $('.station-info-toggle-btn').show();
                $('.station-info-toggle-btn').addClass('visible-xs');
            }

            $('#stationInfoDialog').dialog('open');
            $('.dialog-box.container').scrollTop(0);
        });
    });    
}
