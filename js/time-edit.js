(function ($) {
  Drupal.behaviors.timeRulesEditBehavior = {
    attach: function (context, settings) {
      $(context).find('[class*="0-date-time-rules-time-rules"]').once('time-rules-edit').each(function() {
        var timesContainer = $('<ul>', { 'class': 'times-container'});
        $('[class*="0-date-time-rules-time-rules"]').append(timesContainer);
        initTimeRules($);
      });
    }
  };
})(jQuery);

var timesToShow;

function initTimeRules($) {
    var timeRulesContainer = $('<ul>', { 'class': 'time-rules rules-list'});

    var btnAddTimeRule = $('<input>', {
        id: 'btn-add-time-rule',
        'type': 'button',
        'value': 'Добавить правило',
        'class': 'btn-like-link'
    });
    $(btnAddTimeRule).click(function() {
        var newTimeRule = getNewTimeRuleForm($);
        $(timeRulesContainer).append(newTimeRule);
        cleaveJSInit($);
    });
    
    $('[class*="0-date-time-rules-time-rules"]').append(timeRulesContainer);
    $('[class*="0-date-time-rules-time-rules"]').append(btnAddTimeRule);

    if ($('[id*="0-date-time-rules-time-rules"]').val() !== "") {
        var loadedRules = $('[id*="0-date-time-rules-time-rules"]').val().split("\n");
        addLoadedTimeRules($, loadedRules, timeRulesContainer);
    }

    cleaveJSInit($);
}

function getNewTimeRuleForm($) {
    var selectIncludeOrExclude = $('<select>', {
        'name': 'include_or_exclude'
    });
    $(selectIncludeOrExclude).append('<option value="+">Включить</option>');
    $(selectIncludeOrExclude).append('<option value="-">Исключить</option>');

    var selectRuleType = $('<select>', {
        'name': 'rule_type'
    });
    $(selectRuleType).append('<option value="time">время</option>');
    $(selectRuleType).append('<option value="every n minutes">каждые N минут</option>');

    var inputEveryNMinutes = $('<input>', {
        'type': 'number',
        'min': 2,
        'value': 30,
        'name': 'every_n_minutes'
    });

    var labelBeginTime = $('<span>');
    $(labelBeginTime).text(' с ');

    var inputBeginTime = $('<input>', {
        'type': 'text',
        'name': 'begin_time',
        'class': 'input-time',
        'pattern': '(\\d{1}|[0-1]{1}\\d{1}|2[0-3]{1}):[0-5]{1}\\d{1}',
        'placeholder': '08:00'
    });

    var labelEndTime = $('<span>');
    $(labelEndTime).text(' до ');

    var inputEndTime = $('<input>', {
        'type': 'text',
        'name': 'end_time',
        'class': 'input-time',
        'pattern': '(\\d{1}|[0-1]{1}\\d{1}|2[0-3]{1}):[0-5]{1}\\d{1}',
        'placeholder': '20:00'
    });

    var btnRemoveDateRule = $('<input>', {
        'class': 'btn-remove-time-rule btn-like-link',
        'type': 'button',
        'value': 'Убрать правило'
    });
    
    var newTimeRule = $('<li>', { 'class': 'time-rule-form' });
    $(newTimeRule).append(selectIncludeOrExclude);
    $(newTimeRule).append(selectRuleType);
    $(newTimeRule).append(inputEveryNMinutes);
    $(newTimeRule).append(labelBeginTime);
    $(newTimeRule).append(inputBeginTime);
    $(newTimeRule).append(labelEndTime);
    $(newTimeRule).append(inputEndTime);
    $(newTimeRule).append(btnRemoveDateRule);

    $(selectRuleType).change(function(){
        value = $(this).val();
        switch(value) {
            case 'time':
                $(inputEveryNMinutes).hide();
                $(labelBeginTime).hide();
                $(inputBeginTime).show();
                $(labelEndTime).hide();
                $(inputEndTime).hide();
                break;
            case 'every n minutes':
            default:
                $(inputEveryNMinutes).show();
                $(labelBeginTime).show();
                $(inputBeginTime).show();
                $(labelEndTime).show();
                $(inputEndTime).show();
                break;
        }
    });

    $(selectIncludeOrExclude).change(function(){handleTimeRules($);});
    $(selectRuleType)        .change(function(){handleTimeRules($);});
    $(inputEveryNMinutes)    .change(function(){handleTimeRules($);});
    $(inputBeginTime)        .change(function(){handleTimeRules($);});
    $(inputEndTime)          .change(function(){handleTimeRules($);});
    
    $(selectRuleType).trigger('change');

    $(btnRemoveDateRule).click(function(){
        $(newTimeRule).remove();
        handleTimeRules($);
    });

    return newTimeRule;
}

function handleTimeRules($) {

    $('[id*="0-date-time-rules-time-rules"]').val('');
    timesToShow = [];
    $('.times-container').html('');

    $('.time-rule-form').each(function() {
        var includeOrExclude = $(this).find('select[name="include_or_exclude"]').val();
        var ruleType         = $(this).find('select[name="rule_type"]').val();
        var everyNMinutes    = $(this).find('input[name="every_n_minutes"]').val();
        var beginTime        = $(this).find('input[name="begin_time"]').val();
        var endTime          = $(this).find('input[name="end_time"]').val();

        var re = /^(\d{1}|[0-1]{1}\d{1}|2[0-3]{1}):[0-5]{1}\d{1}$/;

        if (!re.test(beginTime)) return;
        if (ruleType !== 'time' && !re.test(endTime)) return;

        if (beginTime.length === 4) beginTime = '0' + beginTime;
        if (endTime.length === 4)   endTime   = '0' + endTime;

        everyNMinutes = parseInt(everyNMinutes);

        addTimeRuleToTextArea($, includeOrExclude, ruleType, everyNMinutes, beginTime, endTime);
        addTimeRuleToTimeContainer($, includeOrExclude, ruleType, everyNMinutes, beginTime, endTime);
    });

    var temp = $('[id*="0-date-time-rules-time-rules"]').val();
    $('[id*="0-date-time-rules-time-rules"]').val($.trim(temp));
}

function addTimeRuleToTextArea($, includeOrExclude, ruleType, everyNMinutes, beginTime, endTime) {
    var temp = $('[id*="0-date-time-rules-time-rules"]').val();
    temp += includeOrExclude + ruleType + ' ' + beginTime + ' '; 
    temp += ruleType === 'time' ? beginTime : endTime;
    temp += '\n';
    temp = temp.replace(' n ', ' ' + everyNMinutes.toString() + ' ');

    $('[id*="0-date-time-rules-time-rules"]').val(temp);
}

function addTimeRuleToTimeContainer($, includeOrExclude, ruleType, everyNMinutes, beginTime, endTime) {
    switch(ruleType) {
        case 'time':
            if (includeOrExclude === '+') {
                addTimeToTimeContainer(Date.parse(beginTime));
            } else {
                removeTimeFromTimeContainer(Date.parse(beginTime));
            }
            break;
        case 'every n minutes':
        default:
            if (includeOrExclude === '+') {
                var cur = Date.parse(beginTime);
                var maxTime = Date.parse(endTime);
                while (cur <= maxTime) {
                    addTimeToTimeContainer(cur);
                    cur = cur.add(everyNMinutes).minutes();
                }
            } else {
                var cur = Date.parse(beginTime);
                var maxTime = Date.parse(endTime);
                while (cur <= maxTime) {
                    removeTimeFromTimeContainer(cur);
                    cur = cur.add(everyNMinutes).minutes();
                }
            }
            break;
    }
    rerenderTimes($);
}

function addLoadedTimeRules($, loadedRules, timeRulesContainer) {
    for (var i = 0; i < loadedRules.length; i++) {
        var newTimeRule = getNewTimeRuleForm($);
        
        var includeOrExclude = loadedRules[i].substring(0, 1);
        var ruleType         = loadedRules[i].substring(1);
            ruleType         = ruleType.substring(0, ruleType.length - 12).trim();
        var times            = loadedRules[i].substring(loadedRules[i].length - 12).trim().split(' ');
        var everyNMinutes    = ruleType.replace('every ', '').replace(' minutes', '');
            everyNMinutes    = parseInt(everyNMinutes);
        var beginTime        = times[0];
        var endTime          = times[1];
            ruleType         = ruleType.replace(' ' + everyNMinutes + ' ', ' n ');

        $(newTimeRule).find('select[name="include_or_exclude"]').val(includeOrExclude);
        $(newTimeRule).find('select[name="rule_type"]')         .val(ruleType);
        $(newTimeRule).find('input[name="every_n_minutes"]')    .val(everyNMinutes);
        $(newTimeRule).find('input[name="begin_time"]')         .val(beginTime);
        $(newTimeRule).find('input[name="end_time"]')           .val(endTime);

        $(timeRulesContainer).append(newTimeRule);
        $(newTimeRule).find('select[name="rule_type"]').trigger('change');
    }
}

function addTimeToTimeContainer(time) { 
    var needToAdd = true;
    for (var i = 0; i < timesToShow.length; i++) {
        if (timesToShow[i].valueOf() === time.valueOf()) {
            needToAdd = false;
            break;
        }
    }
    if (needToAdd) timesToShow.push(new Date(JSON.parse(JSON.stringify(time))));
}

function removeTimeFromTimeContainer(time) { 
    var index = -1;
    for (var i = 0; i < timesToShow.length; i++) {
        if (timesToShow[i].valueOf() === time.valueOf()) {
            index = i;
            break;
        }
    }
    if (index != -1) timesToShow.splice(index, 1);
}

function rerenderTimes($) {
    $('.times-container').html('');
    timesToShow.sort();
    timesToShow.forEach(function(t) {
        var timeElement = $('<li>', {'class': 'time-element'});
        $(timeElement).text(t.toString('HH:mm'));
        $('.times-container').append(timeElement);
    });
}

function cleaveJSInit($) {
    $('.input-time').toArray().forEach(function(field){
        new Cleave(field, {
            blocks: [2, 2],
            delimiter: ':',
            numericOnly: true
        });
    });
}