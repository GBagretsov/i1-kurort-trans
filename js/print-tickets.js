(function ($) {
  Drupal.behaviors.printTicketsBehavior = {
    attach: function (context, settings) {
      $(context).find('.order-info').once('print-tickets').each(function() {
        jQuery('[id*="btn-print-order-"]').click(function(e) {
          e.preventDefault();
          jQuery(this).parents('[id*="order-id-"]').printThis({
            importCSS: true,
            importStyle: true,
            printDelay: 1000
          });
        });
      });
    }
  };
})(jQuery);