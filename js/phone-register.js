$(function () {
    handlePhoneRegister();
    handlePhoneLogin();
});

function handlePhoneRegister() {
    var $ = jQuery;

    var usernameInput = $('#edit-name');
    $('[name="field_telefon[0][mobile]"]').val($(usernameInput).val());

    $('#edit-field-telefon-wrapper').before($('.form-item-name'));

    $(usernameInput).on('input', function() {
        var entered = $(usernameInput).val();
        var validated = validatePhone(entered).result;
        $('[name="field_telefon[0][mobile]"]').val(validated);
    });

    $(usernameInput).blur(function() {
        var entered = $(usernameInput).val();
        var processed = entered.replace('(', '').replace(')', '');
        $(usernameInput).val(processed);
    });
}

function validatePhone(phone) {
    var newPhone = phone.replace(/[^0-9]+/g, '');            // Убираем всё, кроме цифр
    newPhone = newPhone.replace(/(^8|^7)/, '+7');            // Заменяем 8 и 7 в начале на +7
    if (newPhone.charAt(0) !== '+') newPhone = '+7' + newPhone; // Если всё ещё нет + в начале, то добавляем +7 к началу

    if (newPhone.search(/^\+7[0-9]{10}$/) === -1) { // Если не совпадает
        return {
            valid: false,
            result: phone
        }
    } else {
        return {
            valid: true,
            result: newPhone
        }
    }
}

function handlePhoneLogin() {
    $('#user-login-form #edit-submit').click(function(e) {
        var usernameInput = $('#edit-name');
        var entered = $(usernameInput).val();
        var validated = validatePhone(entered).result;
        $(usernameInput).val(validated);
    });
    $('#user-pass #edit-submit').click(function(e) {
        var usernameInput = $('#edit-name');
        var entered = $(usernameInput).val();
        var validated = validatePhone(entered).result;
        $(usernameInput).val(validated);
    });
}