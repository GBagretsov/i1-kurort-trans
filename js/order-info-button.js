(function ($) {
  Drupal.behaviors.showSidePanelBehavior = {
    attach: function (context, settings) {
      $(context).find('#showSidePanelMarker').once('show-side-panel').each(function() {
        initSidePanel($);
      });
    }
  };
})(jQuery);

var sidePanelLinks = {
    'logged-in': [
        {
            'href':  '/user',
            'class': 'orders-list',
            'text':  'Мои заказы'
        },
        {
            'href':  '/user/logout',
            'class': 'logout',
            'text':  'Выйти из профиля'
        }
    ],
    'anonymous': [
        {
            'href':  '/order-info',
            'class': 'order-info',
            'text':  'Найти свой заказ'
        },
        {
            'href':  '/user/login',
            'class': 'login',
            'text':  'Войти'
        },
        {
            'href':  '/user/register',
            'class': 'register',
            'text':  'Зарегистрироваться'
        }
    ]
}

function initSidePanel($) {
    var sidePanel = $('<div>', { 'class': 'side-panel hidden-xs' });

    if ($('body.user-logged-in').length) {
        for (var i = 0; i < sidePanelLinks['logged-in'].length; i++) {
            var _href  = sidePanelLinks['logged-in'][i]['href'];
            var _class = sidePanelLinks['logged-in'][i]['class'];
            var _text  = sidePanelLinks['logged-in'][i]['text'];

            addLinkToPanelAndMenu(_href, _class, _text, sidePanel);
        }
    } else {
        for (var i = 0; i < sidePanelLinks['anonymous'].length; i++) {
            var _href  = sidePanelLinks['anonymous'][i]['href'];
            var _class = sidePanelLinks['anonymous'][i]['class'];
            var _text  = sidePanelLinks['anonymous'][i]['text'];

            addLinkToPanelAndMenu(_href, _class, _text, sidePanel);
        }
    }

    $('body').append(sidePanel);
}

function addLinkToPanelAndMenu(_href, _class, _text, sidePanel) {
    var $ = jQuery;
    var mainMenu = $('ul.menu--main.navbar-nav');

    $(sidePanel).append(
        $('<a>', {
            'href':  _href,
            'class': _class
        }).text(_text)
    );

    $(mainMenu).append(
        $('<li>', { 'class': 'visible-xs' }).append(
            $('<a>', { 'href': _href }).text(_text)
        )
    );
}
