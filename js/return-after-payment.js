(function ($) {
  Drupal.behaviors.returnAfterPayment = {
    attach: function (context, settings) {
      $(context).find('.show-when-checking').once('return-after-payment').each(function() {
        handleReturn();
      });
    }
  };
})(jQuery);

function handleReturn() {
    var $ = jQuery;

    $('.order-info .panel-heading a').remove();
    $('.order-info .panel-heading form').remove();

    var tries = 0;

    var checkStatus = function() {
        tries++;
        $.get('api/order-info/status/' + getUrlParameter('order_id'))
            .done(function(data) {
                var status = data.status;
                console.log('status: ', status);
                if (status === 2) {
                    $('.order-info .status-text')
                        .removeClass('text-info')
                        .removeClass('text-danger')
                        .addClass('text-success')
                        .text('Оплачен');
                    $('.show-when-checking').remove();
                    $('.show-when-error').remove();
                    $('.show-when-confirmed').removeClass('hidden');
                    $('h1').text('Заказ успешно оплачен');
                    handleNotification();
                } else {
                    onCheckFailed();
                }
            })
            .fail(function() {
                onCheckFailed();
            });
    }

    var onCheckFailed = function() {
        if (tries > 40) {
            $('.show-when-checking').remove();
            $('.show-when-confirmed').remove();
            $('.show-when-error').removeClass('hidden');
            $('h1').text('Произошла ошибка при оплате');
        } else {
            setTimeout(checkStatus, 5000);
        }
    }

    checkStatus();
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};