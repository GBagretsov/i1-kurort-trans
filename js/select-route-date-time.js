(function ($) {
  Drupal.behaviors.selectRouteDateTime = {
    attach: function (context, settings) {
      $(context).find('body').once('select-route-date-time').each(function() {
        initRouteDateSelectCalendar($);
      });
    }
  };
})(jQuery);

function initRouteDateSelectCalendar($) {
    if ($('.select-route-date').length === 0) return;
    pickmeup.defaults.locales['ru'] = {
        days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
    };

    var selectableDates = [];
    var timeElements = $('.route-pattern .time-element');
    $(timeElements).each(function(i, el) {
        if (selectableDates.length === 0 || $(this).data('date') !== selectableDates[selectableDates.length - 1]) {
            selectableDates[selectableDates.length] = $(this).data('date');
        }
    });
    if (selectableDates.length === 0) {
        return;
    } else {
        $('.times-container-wrap').removeClass('hidden');
    }

    selectableDates.sort();

    pickmeup('.select-route-date', {
        position: 'bottom',
        hide_on_select: true,
        format: 'd.m.Y',
        select_month: false,
        select_year: false,
        locale: 'ru',
        class_name: 'select-route-date-datepicker',
        position: function () {
            var offset = $('.select-route-date').offset();
            var left = parseInt(offset.left, 10);
            var top  = parseInt(offset.top,  10) - 45;
            return {
                'left': left + 'px',
                'top':  top  + 'px'
            };
        },
        render: function (date) {
            var selectable = false;
            
            for (var i = 0; i < selectableDates.length; i++) {
                if (Date.parse(selectableDates[i]).valueOf() === date.valueOf()) {
                    selectable = true;
                    break;
                }
            }
            if (!selectable) {
                return { disabled: true };
            }
            return {};
        } 
    });

    $('.select-route-date').on('pickmeup-change', function (e) {
        var newDate = e.detail.date.toString('yyyy-MM-dd');
        $(timeElements).hide();
        $('.route-pattern .time-element[data-date="' + newDate + '"]').show();
        $('.route-pattern .time-element[data-date="' + newDate + '"]').first().click();
    });

    $(timeElements).click(function() {
        $(timeElements).removeClass('active');
        $(this).addClass('active');
        var routeId = $(this).data('route-id');
        $('#buyTicket').attr('href', '/route/' + routeId);
    });

    $(timeElements).hide();
    $('.route-pattern .time-element[data-date="' + selectableDates[0] + '"]').show();
    pickmeup('.select-route-date').set_date(Date.parse(selectableDates[0]));
    $('.route-pattern .time-element[data-date="' + selectableDates[0] + '"]').first().click();

    $('.times-container').removeClass('hidden');
}