(function ($) {
  Drupal.behaviors.dateRulesEditBehavior = {
    attach: function (context, settings) {
      $(context).find('[class*="0-date-time-rules-date-rules"]').once('date-rules-edit').each(function() {
        initPermanentDates($);
        initCalendar($);
        initDateRules($);
      });
    }
  };
})(jQuery);

var minDate = Date.today();
var maxDate = Date.today().add(2).months().moveToLastDayOfMonth();

var permanentDates;
var currentDates = [];

function initCalendar($) {
    var calendar = $('<div>', { 'class': 'date-preview'});
    $('[class*="0-date-time-rules-date-rules"]').append(calendar);

    pickmeup.defaults.locales['ru'] = {
        days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
    };

    pickmeup('.date-preview', {
        format: 'd-m-Y',
        mode: 'multiple',
        select_month: false,
        select_year: false,
        calendars: 3,
        flat: true,
        min: minDate,
        max: maxDate,
        locale: 'ru',
        render: function (date) {
            var permanent = false;
            for (var i = 0; i < permanentDates.length; i++) {
                if (Date.parse(permanentDates[i]).valueOf() === date.valueOf()) {
                    permanent = true;
                    break;
                }
            }
            if (permanent) return { selected: true, class_name: 'permanent-date' };
            return {};
        } 
    });

    pickmeup('.date-preview').clear();

    $('.date-preview').on('pickmeup-change', function (e) {
        var newDates = e.detail.date;
        var diff = arraysDiff(newDates, currentDates)[0];
        diff = Date.parse(diff.substring(0, 15));

        var newDateRule = getNewDateRuleForm($);
        $(newDateRule).find('select[name="rule_type"]')     .val('date');
        $(newDateRule).find('input[name="date"]')           .val(diff.toString('yyyy-MM-dd'));
        $(newDateRule).find('input[name="every_nth_day"]')  .val(2);
        $(newDateRule).find('input[name="begin_with_date"]').val(diff.toString('yyyy-MM-dd'));
            
        if (newDates.length > currentDates.length) { // Добавили дату
            $(newDateRule).find('select[name="include_or_exclude"]').val('+');
        } else { // Убрали дату, и она не красная
            var permanent = false;
            for (var i = 0; i < permanentDates.length; i++) {
                if (Date.parse(permanentDates[i]).valueOf() === diff.valueOf()) {
                    permanent = true;
                    break;
                }
            }
            if (permanent) return;
            $(newDateRule).find('select[name="include_or_exclude"]').val('-');
        }

        $('.date-rules').append(newDateRule);
        $(newDateRule).find('select[name="rule_type"]').trigger('change');
    });
}

function initPermanentDates($) {
    permanentDates = $('[name*="[0][sale_opened_dates]"]').val();
    permanentDates = JSON.parse(permanentDates);
}

function initDateRules($) {
    var dateRulesContainer = $('<ul>', { 'class': 'date-rules rules-list'});

    var btnAddDateRule = $('<input>', {
        id: 'btn-add-date-rule',
        'type': 'button',
        'value': 'Добавить правило',
        'class': 'btn-like-link'
    });
    $(btnAddDateRule).click(function() {
        var newDateRule = getNewDateRuleForm($);
        $(dateRulesContainer).append(newDateRule);
    });
    
    $('[class*="0-date-time-rules-date-rules"]').append(dateRulesContainer);
    $('[class*="0-date-time-rules-date-rules"]').append(btnAddDateRule);

    if ($('[id*="0-date-time-rules-date-rules"]').val() !== "") {
        var loadedRules = $('[id*="0-date-time-rules-date-rules"]').val().split("\n");
        addLoadedDateRules($, loadedRules, dateRulesContainer);
    }
}

function getNewDateRuleForm($) {
    var selectIncludeOrExclude = $('<select>', {
        'name': 'include_or_exclude'
    });
    $(selectIncludeOrExclude).append('<option value="+">Включить</option>');
    $(selectIncludeOrExclude).append('<option value="-">Исключить</option>');

    var selectRuleType = $('<select>', {
        'name': 'rule_type'
    });
    $(selectRuleType).append('<option value="date">дату</option>');
    $(selectRuleType).append('<option value="every 1 days">каждый день</option>');
    $(selectRuleType).append('<option value="every n days">каждый N-й день</option>');
    $(selectRuleType).append('<option value="every monday">каждый понедельник</option>');
    $(selectRuleType).append('<option value="every tuesday">каждый вторник</option>');
    $(selectRuleType).append('<option value="every wednesday">каждую среду</option>');
    $(selectRuleType).append('<option value="every thursday">каждый четверг</option>');
    $(selectRuleType).append('<option value="every friday">каждую пятницу</option>');
    $(selectRuleType).append('<option value="every saturday">каждую субботу</option>');
    $(selectRuleType).append('<option value="every sunday">каждое воскресенье</option>');

    var inputEveryNthDay = $('<input>', {
        'type': 'number',
        'min': 2,
        'value': 2,
        'name': 'every_nth_day'
    });

    var labelBeginWithDate = $('<span>');
    $(labelBeginWithDate).text(' с ');

    var inputBeginWithDate = $('<input>', {
        'type': 'date',
        'name': 'begin_with_date'
    });

    var labelEndDate = $('<span>');
    $(labelEndDate).text(' по ');

    var inputEndDate = $('<input>', {
        'type': 'date',
        'name': 'end_date'
    });

    var btnRemoveDateRule = $('<input>', {
        'class': 'btn-remove-date-rule btn-like-link',
        'type': 'button',
        'value': 'Убрать правило'
    });
    
    var newDateRule = $('<li>', { 'class': 'date-rule-form' });
    $(newDateRule).append(selectIncludeOrExclude);
    $(newDateRule).append(selectRuleType);
    $(newDateRule).append(inputEveryNthDay);
    $(newDateRule).append(labelBeginWithDate);
    $(newDateRule).append(inputBeginWithDate);
    $(newDateRule).append(labelEndDate);
    $(newDateRule).append(inputEndDate);
    $(newDateRule).append(btnRemoveDateRule);

    $(selectRuleType).change(function(){
        value = $(this).val();
        switch(value) {
            case 'date':
                $(inputEveryNthDay).hide();
                $(labelBeginWithDate).hide();
                $(inputBeginWithDate).show();
                $(labelEndDate).hide();
                $(inputEndDate).hide();
                break;
            case 'every n days':
                $(inputEveryNthDay).show();
                $(labelBeginWithDate).show();
                $(inputBeginWithDate).show();
                $(labelEndDate).show();
                $(inputEndDate).show();
                break;
            default:
                $(inputEveryNthDay).hide();
                $(labelBeginWithDate).show();
                $(inputBeginWithDate).show();
                $(labelEndDate).show();
                $(inputEndDate).show();
                break;
        }
    });

    $(selectIncludeOrExclude).change(function(){handleDateRules($);});
    $(selectRuleType)        .change(function(){handleDateRules($);});
    $(inputEveryNthDay)      .change(function(){handleDateRules($);});
    $(inputBeginWithDate)    .change(function(){handleDateRules($);});
    $(inputEndDate)          .change(function(){handleDateRules($);});
    
    $(selectRuleType).trigger('change');

    $(btnRemoveDateRule).click(function(){
        $(newDateRule).remove();
        handleDateRules($);
    });

    return newDateRule;
}

function handleDateRules($) {

    $('[id*="0-date-time-rules-date-rules"]').val('');
    pickmeup('.date-preview').clear();
    currentDates = [];

    $('.date-rule-form').each(function() {
        var includeOrExclude = $(this).find('select[name="include_or_exclude"]').val();
        var ruleType         = $(this).find('select[name="rule_type"]').val();
        var everyNthDay      = $(this).find('input[name="every_nth_day"]').val();
        var beginWithDate    = $(this).find('input[name="begin_with_date"]').val();
        var endDate          = $(this).find('input[name="end_date"]').val();

        if (beginWithDate === '') return;
        if (ruleType !== 'date' && endDate === '') endDate = '2038-01-01'; // Симуляция бесконечного диапазона
        if (ruleType === 'every 1 days') everyNthDay = 1;
        everyNthDay = parseInt(everyNthDay);

        addDateRuleToTextArea($, includeOrExclude, ruleType, everyNthDay, beginWithDate, endDate);
        addDateRuleToDatesArray($, includeOrExclude, ruleType, everyNthDay, beginWithDate, endDate);
    });

    var temp = $('[id*="0-date-time-rules-date-rules"]').val();
    $('[id*="0-date-time-rules-date-rules"]').val($.trim(temp));

    pickmeup('.date-preview').set_date(currentDates);
}

function addDateRuleToTextArea($, includeOrExclude, ruleType, everyNthDay, beginWithDate, endDate) {
    var temp = $('[id*="0-date-time-rules-date-rules"]').val();
    temp += includeOrExclude + ruleType + ' '; 
    temp += beginWithDate + ' ';
    temp += ruleType === 'date' ? beginWithDate : endDate;
    temp += '\n';
    temp = temp.replace(' n ', ' ' + everyNthDay.toString() + ' ');

    $('[id*="0-date-time-rules-date-rules"]').val(temp);
}

function addDateRuleToDatesArray($, includeOrExclude, ruleType, everyNthDay, beginWithDate, endDate) {
    var _endDate = Date.parse(endDate);
    switch(ruleType) {
        case 'date':
            if (includeOrExclude === '+') {
                addDateToDatesArray(Date.parse(beginWithDate));
            } else {
                removeDateFromDatesArray(Date.parse(beginWithDate));
            }
            break;
        case 'every 1 days':
        case 'every n days':
            if (includeOrExclude === '+') {
                var cur = Date.parse(beginWithDate);
                while (cur <= maxDate && cur <= _endDate) {
                    addDateToDatesArray(cur);
                    cur = cur.add(everyNthDay).days();
                }
            } else {
                var cur = Date.parse(beginWithDate);
                while (cur <= maxDate && cur <= _endDate) {
                    removeDateFromDatesArray(cur);
                    cur = cur.add(everyNthDay).days();
                }
            }
            break;
        default:
            if (includeOrExclude === '+') {
                var dayOfWeek = Date.getDayNumberFromName(ruleType.replace('every ', ''));
                var cur = Date.parse(beginWithDate).add(-1).days().moveToDayOfWeek(dayOfWeek);
                while (cur <= maxDate && cur <= _endDate) {
                    addDateToDatesArray(cur);
                    cur = cur.add(7).days();
                }
            } else {
                var dayOfWeek = Date.getDayNumberFromName(ruleType.replace('every ', ''));
                var cur = Date.parse(beginWithDate).add(-1).days().moveToDayOfWeek(dayOfWeek);
                while (cur <= maxDate && cur <= _endDate) {
                    removeDateFromDatesArray(cur);
                    cur = cur.add(7).days();
                }
            }
            break;
    }
}

function addDateToDatesArray(date) {
    if (date < minDate || date > maxDate) return;
    var needToAdd = true;
    for (var i = 0; i < currentDates.length; i++) {
        if (currentDates[i].valueOf() === date.valueOf()) {
            needToAdd = false;
            break;
        }
    }
    if (needToAdd) currentDates.push(new Date(date.getTime()));
}

function removeDateFromDatesArray(date) {
    var index = -1;
    for (var i = 0; i < currentDates.length; i++) {
        if (currentDates[i].valueOf() === date.valueOf()) {
            index = i;
            break;
        }
    }
    if (index != -1) currentDates.splice(index, 1);
}

function addLoadedDateRules($, loadedRules, dateRulesContainer) {
    for (var i = 0; i < loadedRules.length; i++) {
        var newDateRule = getNewDateRuleForm($);
        
        var includeOrExclude = loadedRules[i].substring(0, 1);
        var ruleType         = loadedRules[i].substring(1);
            ruleType         = ruleType.substring(0, ruleType.length - 22).trim();
        var dates            = loadedRules[i].substring(loadedRules[i].length - 22).trim().split(' ');
        var everyNthDay      = ruleType.replace('every ', '').replace(' days', '');
            everyNthDay      = parseInt(everyNthDay);
        var beginWithDate    = dates[0];
        var endDate          = dates[1];
        if (everyNthDay >= 2) {
            ruleType = ruleType.replace(" " + everyNthDay + " ", " n ");
        } else {
            everyNthDay = 2; // Для формы валидации (N >= 2)
        }

        $(newDateRule).find('select[name="include_or_exclude"]').val(includeOrExclude);
        $(newDateRule).find('select[name="rule_type"]')         .val(ruleType);
        $(newDateRule).find('input[name="every_nth_day"]')      .val(everyNthDay);
        $(newDateRule).find('input[name="begin_with_date"]')    .val(beginWithDate);
        if (endDate !== '2038-01-01') $(newDateRule).find('input[name="end_date"]').val(endDate);

        $(dateRulesContainer).append(newDateRule);
        $(newDateRule).find('select[name="rule_type"]').trigger('change');
    }
}

function arraysDiff (a1, a2) {

    var a = [], diff = [];

    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }

    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }

    for (var k in a) {
        diff.push(k);
    }

    return diff;
}