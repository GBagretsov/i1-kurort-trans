(function ($) {
  Drupal.behaviors.notifyOrder = {
    attach: function (context, settings) {
      $(context).find('.order-notification').once('notify-order').each(function() {
        if ($('#deferred').length === 1) {
            return;
        }
        handleNotification();
      });
    }
  };
})(jQuery);

function handleNotification() {
    var $ = jQuery;
    var orderNotification = $('#orderNotification');
    $(orderNotification).removeClass('hidden');

    var orderId = $(orderNotification).data('order-id');
    var orderHash = $(orderNotification).data('order-hash');

    var btnChoosePhone = $(orderNotification).find('.btn-primary');

    $(btnChoosePhone).click(function () {
        $(this).prop('disabled', true);
        var phoneIndex = $(orderNotification).find('select').val() || 0;
        $.post('/api/order-info/send/' + orderId + '/' + phoneIndex, {
            hash: orderHash
        }).done(function(data) {
            if (data.status === 'success') {
                showSuccess();
            } else {
                showError();
            }
        }).fail(function() {
            showError();
        }).always(function() {
            $(btnChoosePhone).prop('disabled', false);
        });
    });

    if ($(orderNotification).find('option').length === 1) {
        console.log('single phone');
        $(btnChoosePhone).trigger('click');
    }

    function showError() {
        $(orderNotification).find('.alert').remove();
        var errorMessage = $('\
            <div class="alert alert-danger fade" role="alert">\
                Произошла ошибка. Попробуйте повторить позднее.\
            </div>\
        ');
        $(orderNotification).append(errorMessage);
        setTimeout(function () {
            $(errorMessage).addClass('in');
        }, 100);
    }

    function showSuccess() {
        $(orderNotification).html('');
        var successMessage = $('\
            <div class="alert alert-success fade" role="alert">\
                SMS с информацией о заказе отправлено\
            </div>\
        ');
        $(orderNotification).append(successMessage);
        setTimeout(function () {
            $(successMessage).addClass('in');
        }, 100);
    }
}
