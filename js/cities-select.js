(function ($) {
  Drupal.behaviors.citiesSelectBehavior = {
    attach: function (context, settings) {
      $(context).find('.cities-select').once('cities-select').each(function() {
        initCitiesSelect($);
      });
    }
  };
})(jQuery);

function initCitiesSelect($) {
    var departureCityId = $('.ride-thread__row').first().data('city-id');
    var arrivalCityId   = $('.ride-thread__row').last() .data('city-id');

    var departureCityStations = $('.ride-thread__row[data-city-id="' + departureCityId + '"]');
    var arrivalCityStations   = $('.ride-thread__row[data-city-id="' + arrivalCityId   + '"]');

    var dpt_needToAddMapBtn = false;
    var arl_needToAddMapBtn = false;

    handleMapIfNeeded(departureCityStations, 'Выбрать пункт отправления', 'Отправление', '#dptDialog', 'dptMap');
    handleMapIfNeeded(arrivalCityStations,   'Выбрать пункт прибытия',    'Прибытие',    '#arlDialog', 'arlMap');

    $(departureCityStations).not(':first').remove();
    $(arrivalCityStations)  .not(':first').remove();

    // Вокзалы по умолчанию
    if ($(departureCityStations).first().data('station-id')) {
        $('input[name="from_id"]').val($(departureCityStations).first().data('station-id'));
    }
    if ($(arrivalCityStations)  .first().data('station-id')) {
        $('input[name="to_id"]')  .val($(arrivalCityStations)  .first().data('station-id'));
    }
}

function handleMapIfNeeded(stations, labelForMultipleStations, timeLabel, dialogWindowId, mapId) {
    var $ = jQuery;
    var needToAddMapBtn = false;

    if ($(stations).length === 1) {
        var singleStationLat  = parseFloat($(stations).first().data('latitude'));
        var singleStationLong = parseFloat($(stations).first().data('longitude'));
        if (!isNaN(singleStationLat)) {
            var buttonLabel = 'Показать на карте';
            needToAddMapBtn = true;
            ymaps.ready(function() {
                var coords = [singleStationLat, singleStationLong];
                var myMap = new ymaps.Map(mapId, {
                    center: coords, 
                    zoom: 17,
                    controls: ['zoomControl']
                });
                var hint = $(stations).first().find('.ride-thread__subtitle').text().trim();
                if (hint === '') {
                    hint = $(stations).first().find('.ride-thread__title').text().trim();
                }
                var cityMarker = new ymaps.Placemark(coords, {
                    hintContent: hint
                });
                myMap.geoObjects.add(cityMarker);
            });
        }
    } else {
        var buttonLabel = labelForMultipleStations;
        needToAddMapBtn = true;
        ymaps.ready(function() {
            handleMultipleStations(stations, mapId, timeLabel, dialogWindowId);
        });
    }

    if (needToAddMapBtn) {
        var showMapBtn = $('<td>', {
            'class': 'ride-thread__col'
        });
        $(showMapBtn).append($('<button>', {
            'type': 'button',
            'class': 'btn-like-link'
        }).text(buttonLabel));

        $(stations).first().append(showMapBtn);
        
        $(showMapBtn).click(function(e) {
            e.preventDefault();
            $(dialogWindowId).dialog('open');
        });
    }

    $(dialogWindowId).dialog({
        autoOpen: false,
        modal: true,
        draggable: false,
        height: 600,
        width: 1250,
        resizable: false,
        clickOut: true,
        responsive: true,
        dialogClass: 'my-dialog',
        scaleH: 0.95,
        scaleW: 0.95
    });
}

function handleMultipleStations(stations, mapId, timeLabel, dialogWindowId) {
    var $ = jQuery;
    var stationsListId          = mapId + 'StationsList';
    var stationsListContainerId = mapId + 'StationsListContainer';

    $('#' + mapId).before($('<div>', {
        'class': 'stations-list-container',
        'id': stationsListContainerId
    }));

    $('#' + stationsListContainerId).append($('<ul>', {
        'class': 'stations-list',
        'id': stationsListId
    }));

    var myMap = new ymaps.Map(mapId, {
        center: [0.0, 0.0], 
        zoom: 10,
        controls: ['zoomControl']
    });
    
    $(stations).each(function(i, el) {
        var coords = [
            parseFloat($(this).data('latitude')),
            parseFloat($(this).data('longitude'))
        ];

        var id = parseInt($(this).data('station-id'));
        var stationIndex = i + 1;
        var stationName = $(this).find('.ride-thread__subtitle').text();

        var time = $(this).find('.ride-thread__time').text();
        var stationListElement = $('<li>').html(
            '<b>' + stationIndex + '. ' + stationName + '</b><br>' + timeLabel + ': <b>' + time + '</b>'
        );

        if (!isNaN(coords[0])) {        

            var stationMarker = new ymaps.Placemark(coords, {
                iconContent: stationIndex,
                hintContent: stationName
            }, {
                preset: 'islands#blueStretchyIcon'
            });

            stationMarker.events.add('click', function() {
                setStation(id, mapId, stationName, time);
                $(dialogWindowId).dialog('close');
            });

            stationMarker.events.add('mouseenter', function() {
                $(stationListElement).addClass('hover');
                stationMarker.options.set('preset', 'islands#redStretchyIcon');
            });

            stationMarker.events.add('mouseleave', function() {
                $(stationListElement).removeClass('hover');
                stationMarker.options.set('preset', 'islands#blueStretchyIcon');
            });

            $(stationListElement).on('mouseenter', function() {
                stationMarker.options.set('preset', 'islands#redStretchyIcon');
            });

            $(stationListElement).on('mouseleave', function() {
                stationMarker.options.set('preset', 'islands#blueStretchyIcon');
            });
            
            myMap.geoObjects.add(stationMarker);
        }

        $(stationListElement).click(function() {
            setStation(id, mapId, stationName, time);
            $(dialogWindowId).dialog('close');
        });

        $('#' + stationsListId).append(stationListElement);
    });
    if (myMap.geoObjects.getLength() > 0) {
        myMap.events.add('sizechange', function() {
            myMap.setBounds(myMap.geoObjects.getBounds(), { checkZoomRange: true });
        });
    }
}

function setStation(stationId, mapId, stationName, stationTime) {
    if (mapId === 'dptMap') {
        $('.ride-thread__row').first().find('.ride-thread__subtitle').text(stationName);
        $('.ride-thread__row').first().find('.ride-thread__time')    .text(stationTime);
        $('input[name="from_id"]').val(stationId);
    }
    if (mapId === 'arlMap') {
        $('.ride-thread__row').last() .find('.ride-thread__subtitle').text(stationName);
        $('.ride-thread__row').last() .find('.ride-thread__time')    .text(stationTime);
        $('input[name="to_id"]').val(stationId);
    }
}