(function ($) {
  Drupal.behaviors.openedRoutesEditBehavior = {
    attach: function (context, settings) {
      $(context).find('.opened-routes-list').once('opened-routes-edit').each(function() {
        initOpenedRoutesEdit($);
        $('#refresh').click(function(e) {
          var page = $('.page.current').text();
          e.preventDefault();
          $.get(e.target.href).done(function(data) {
            $('.opened-routes-list').html(data.result);
            initOpenedRoutesEdit($, page);
          });
        });
        initDialog($);
      });
    }
  };
})(jQuery);

function initOpenedRoutesEdit($, page) {
    $('.opened-routes-list a').each(function(index) {
        setLinkHandler($, $(this));
    });
    $('.easyPaginateNav').remove();
    $('#opened-routes').easyPaginate({
        paginateElement: 'li',
        elementsPerPage: 45
    });
    if (page) $('a[href="#page:' + page + '"]').trigger('click');
}

function setLinkHandler($, linkElement) {
    $(linkElement).click(function(e) {
        e.preventDefault();
        var link = e.target.href;
        if (link.includes('open')) {
            if (confirm('Вы уверены, что хотите открыть этот рейс?')) {
                $.post(e.target.href).done(function(data) {
                    if (data.result !== 'ok') {
                        alert('Произошла ошибка');
                        return;
                    }
                    $(linkElement).prev().text('Открыт, билетов продано: ' + data.tickets_sold).attr('class', 'opened-route');
                    $(linkElement).text('Отменить рейс').attr('href', '/routes/cancel/' + data.id);
                });
            }
        }
        if (link.includes('cancel')) {
            if (confirm('Вы уверены, что хотите отменить этот рейс?')) {
                $.post(e.target.href).done(function(data) {
                    if (data.result !== 'ok') {
                        $(linkElement).prev().text('Открыт, билетов продано: ' + data.tickets_sold);
                        $(linkElement).remove();
                        alert('Ошибка: нельзя отменить рейс, если на него проданы билеты');
                        return;
                    }
                    $(linkElement).prev().text('Отменён').attr('class', 'cancelled-route');
                    $(linkElement).text('Открыть рейс').attr('href', '/routes/open/' + data.id);
                });
            }
        }
    });
}

function initDialog($) {
    $('#dialog').dialog({
        autoOpen: false,
        modal: true,
        draggable: false,
        height: 600,
        width: 1250,
        resizable: false
    });

    $('#opener').click(function(e) {
        e.preventDefault();
        $('#dialog').dialog('open');
    });
}