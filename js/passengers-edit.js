(function ($) {
  Drupal.behaviors.passengersEdit = {
    attach: function (context, settings) {
      $(context).find('#passengers-data').once('passengers-edit').each(function() {
        handleAddPassengerButton();
        handleExistingRemoveButtons();
        handleValidationOnFormSubmit();
      });
    }
  };
})(jQuery);

var maxPassengers = parseInt(jQuery('#btn-add-passenger').data('max-passengers'));

function handleAddPassengerButton() {
    var $ = jQuery;
    $('#btn-add-passenger').click(function() {
        var newPassenger = $('<div>');
        var newPassengerPhone = $('<input>', {
            'class': 'route-input passenger-data',
            'type': 'tel',
            'name': 'phones[]',
            'placeholder': 'в любом формате',
            'required': true,
        });
        var newPassengerDeleteBtn = $('<button>', {
            'class': 'btn-like-link',
            'tabindex': '-1',
            'type': 'button'
        }).text('Удалить');
        $(newPassengerDeleteBtn).click(function() {
            $(newPassenger).remove();
            showOrHideAddPassengerButton();
        });
        $(newPassenger).append(newPassengerPhone);
        $(newPassenger).append(newPassengerDeleteBtn);
        $(this).before(newPassenger);
        showOrHideAddPassengerButton();
    });
    showOrHideAddPassengerButton();
}

function handleExistingRemoveButtons() {
    var $ = jQuery;
    $('.btnRemovePassenger').each(function(i, el) {
        $(this).click(function() {
            $(this).parent().remove();
            showOrHideAddPassengerButton();
        });
    });
}

function handleValidationOnFormSubmit() {
    var $ = jQuery;
    $('#passengers-data').on('submit', function(event) {
        if (phonesInvalid()) {
            event.preventDefault();
            event.stopPropagation();
        }
    });
}

function phonesInvalid() {
    var $ = jQuery;
    $('.alert-danger').remove();
    var phonesElements = $('input[name="phones[]"]');
    var alertBlock = $('<div>', {
        'class': 'alert alert-danger'
    });
    var allPhonesValid = true;
    $('input[name="phones[]"]').each(function(i, el) {
        var phone = $(this).val().trim();
        phone = phone.replace(/[^0-9]+/g, '');             // Убираем всё, кроме цифр
        phone = phone.replace(/(^8|^7)/, '+7');            // Заменяем 8 и 7 в начале на +7
        if (phone.charAt(0) !== '+') phone = '+7' + phone; // Если всё ещё нет + в начале, то добавляем +7 к началу
        if (phone.search(/^\+7[0-9]{10}$/) === -1) { // Если не совпадает
            allPhonesValid = false;
            $(alertBlock).append($('<p>').text('Это не похоже на номер телефона: ' + $(this).val()));
        }
    });
    if (!allPhonesValid) {
        $('#passengers-data h3').after(alertBlock);
        $('html, body').animate({
            scrollTop: $('#passengers-data h3').offset().top - 70
        }, 500);
    }
    return !allPhonesValid;
}

function showOrHideAddPassengerButton() {
    var $ = jQuery;
    var currentPassengers = $('input[name="phones[]"]').length;
    if (currentPassengers >= maxPassengers) {
        $('#btn-add-passenger').hide();
    } else {
        $('#btn-add-passenger').show();
    }
}