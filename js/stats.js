(function ($) {
  Drupal.behaviors.showStatsBehavior = {
    attach: function (context, settings) {
      $(context).find('.stats-filters').once('show-stats').each(function() {
        handleShowStatsButton($);
        handleRoutesNames($);
        handleSort($);
      });
    }
  };
})(jQuery);

var tooManyResults = 'Показаны не все подходящие рейсы; попробуйте уточнить критерии поиска';

function handleShowStatsButton($) {
    $('#btnShowStats').click(function() {
        $(this).attr('disabled', 'disabled');
        $('#btnShowStats').addClass('disabled');
        $('#btnShowStats').text('Загрузка...');
        $('#statsResult tr').not(':first-child').remove();

        var beginDate = $('input[name="begin_date"]').val();
        var endDate   = $('input[name="end_date"]')  .val();
        var type      = $('select[name="type"]')     .val();
        var routeId   = $('select[name="route_id"]') .val();

        var options = { };

        if (beginDate !== '')    options.begin_date = beginDate;
        if (endDate   !== '')    options.end_date   = endDate;
        if (type      !== 'all') options.type       = type;
        if (routeId   !== '0')   options.route_id   = routeId;

        $.get('/api/stats', options, function(data) {
            for (var i = 0; i < data.length; i++) {
                var row = $('<tr>');
                if (data[i] === 'break') {
                    $(row).append('<td colspan="5">' + tooManyResults + '</td>');
                    $('#statsResult').append(row);
                    continue;
                }
                $(row).append($('<td>').text(data[i].date_time.slice(0, -3)));
                $(row).append($('<td>').text(data[i].id));
                $(row).append($('<td>').text(data[i].name));
                $(row).append($('<td>').text(data[i].tickets_sold + ' из ' + data[i].tickets_amount));
                $(row).append($('<td>').html(parseInt(data[i].earnings) === 0 ? '&mdash;' : data[i].earnings));
                if (data[i].earnings > 0) {
                    var a = $('<a>', {
                        'href': '/admin/stats/' + data[i].id
                    }).text('Пассажиры');
                    $(row).append($('<td>').append(a));
                }
                $('#statsResult').append(row);
            }
            if (data.length === 0) $('#statsResult').append('<tr><td colspan="6">Не найдено</td></tr>');
            $('#btnShowStats').attr('disabled', false);
            $('#btnShowStats').text('Показать');
            $('#btnShowStats').removeClass('disabled');
        });
    });
}

function handleRoutesNames($) {
    var routesNamesContainer = $('#routeName');
    $.get('/api/route/names', function(data) {
        $('select[name="type"]').change(function() {
            $('#routeName option').not('[value="0"]').remove();
            var type = $(this).val();
            for (var i = 0; i < data.length; i++) {
                if (type === 'all' || data[i].type === type) {
                    var option = $('<option>', {
                        'value': data[i].id
                    }).text(data[i].name);
                    $(routesNamesContainer).append(option);
                }
            }
            $(routesNamesContainer).trigger('chosen:updated');
        });
        $(routesNamesContainer).chosen();
        $('select[name="type"]').trigger('change');
    });
}

function handleSort($) {
    $('th.date-time').click(function() {
        var sortOrder = $(this).data('sort-order') || 'asc';
        $(this).data('sort-order', sortOrder === 'asc' ? 'desc' : 'asc');
        var rows = $('#statsResult tr').not(':first-child');
        rows.sort(sortOrder === 'asc' ? sortByDateTimeAsc : sortByDateTimeDesc);
        $('#statsResult').append(rows);
    });
    $('th.name').click(function() {
        var sortOrder = $(this).data('sort-order') || 'asc';
        $(this).data('sort-order', sortOrder === 'asc' ? 'desc' : 'asc');
        var rows = $('#statsResult tr').not(':first-child');
        rows.sort(sortOrder === 'asc' ? sortByNameAsc : sortByNameDesc);
        $('#statsResult').append(rows);
    });
}

function sortByDateTimeAsc(a, b) {
    var $ = jQuery;
    var a_DateTime = $(a).find('td').first().text();
    var b_DateTime = $(b).find('td').first().text();
    if (a_DateTime === tooManyResults) {
        return 1;
    }
    if (b_DateTime === tooManyResults) {
        return -1;
    }
    if (a_DateTime < b_DateTime) {
        return -1;
    }
    if (a_DateTime > b_DateTime) {
        return 1;
    }
    return 0;
}

function sortByDateTimeDesc(a, b) {
    var $ = jQuery;
    var a_DateTime = $(a).find('td').first().text();
    var b_DateTime = $(b).find('td').first().text();
    if (a_DateTime === tooManyResults) {
        return 1;
    }
    if (b_DateTime === tooManyResults) {
        return -1;
    }
    if (a_DateTime > b_DateTime) {
        return -1;
    }
    if (a_DateTime < b_DateTime) {
        return 1;
    }
    return 0;
}

function sortByNameAsc(a, b) {
    var $ = jQuery;
    var a_Name = $(a).find('td:eq(2)').first().text();
    var b_Name = $(b).find('td:eq(2)').first().text();
    if (!a_Name) {
        return 1;
    }
    if (!b_Name) {
        return -1;
    }
    if (a_Name < b_Name) {
        return -1;
    }
    if (a_Name > b_Name) {
        return 1;
    }
    return sortByDateTimeAsc(a, b);
}

function sortByNameDesc(a, b) {
    var $ = jQuery;
    var a_Name = $(a).find('td:eq(2)').first().text();
    var b_Name = $(b).find('td:eq(2)').first().text();
    if (!a_Name) {
        return 1;
    }
    if (!b_Name) {
        return -1;
    }
    if (a_Name > b_Name) {
        return -1;
    }
    if (a_Name < b_Name) {
        return 1;
    }
    return sortByDateTimeAsc(a, b);
}