(function ($) {
  Drupal.behaviors.citiesRulesEditBehavior = {
    attach: function (context, settings) {
      $(context).find('[class*="0-route-route"]').once('cities-rules-edit').each(function() {
        initCitiesFullList($);
        initCitiesContainer($);
      });
    }
  };
})(jQuery);

var citiesFullList;

function initCitiesFullList($) {
    citiesFullList = JSON.parse($('[name*="[0][cities_list]"]').val());
}

function initCitiesContainer($) {

    var nodeType = $('[name*="[0][node_type]"]').val();

    var citiesHeader = $('<ul>', { 'class': 'cities-header rules-list'});
    $(citiesHeader).append($('<li>').text('Населённый пункт'));
    switch (nodeType) {
        case 'mezdugorodnii_reis':
            $(citiesHeader).append($('<li>').text('Вокзал'));
            break;
        case 'ekskursia':
            $(citiesHeader).append($('<li>').text('Достопримечательность'));
            break;
        default:
            $(citiesHeader).append($('<li>').text('Подпункт'));
            break;
    }
    switch (nodeType) {
        case 'tur':
            $(citiesHeader).append($('<li>', { 'class': 'header-item-longer'  }).text('+Время'));
            $(citiesHeader).append($('<li>', { 'class': 'header-item-shorter' }).text('+Расст-е, км'));
            $(citiesHeader).append($('<li>', { 'class': 'header-item-longer'  }).text('+Остановка'));
            break;
        default:
            $(citiesHeader).append($('<li>').text('+Время, мин'));
            $(citiesHeader).append($('<li>').text('+Расст-е, км'));
            if (nodeType === 'mezdugorodnii_reis') {
                $(citiesHeader).append($('<li>').text('+Стоимость, руб'));
            }
            $(citiesHeader).append($('<li>').text('+Остановка, мин'));
            break;
    }

    var citiesContainer = $('<ol>', {
        id: 'cities-container',
        'class': 'cities-container rules-list'
    });

    var btnAddCity = $('<input>', {
        id: 'btn-add-city',
        'type': 'button',
        'value': 'Добавить пункт',
        'class': 'btn-like-link'
    });
    $(btnAddCity).click(function() {
        var newCity = getNewCityForm($);
        $(citiesContainer).append(newCity);
        handleCities($);
    });
    
    $('[class*="0-route-route"]').append(citiesHeader);
    $('[class*="0-route-route"]').append(citiesContainer);
    $('[class*="0-route-route"]').append(btnAddCity);

    if ($('[id*="0-route-route"]').val() !== "") {
        var loadedCities = $('[id*="0-route-route"]').val().split("\n");
        addLoadedCitiesRules($, loadedCities, citiesContainer);
    } else {
        // Добавляем сразу два города
        $(btnAddCity).trigger('click');
        $(btnAddCity).trigger('click');
    }

    var el = document.getElementById('cities-container');
    var sortable = new Sortable(el, {
        handle: '.sort-handle-marker',
        ghostClass: 'sortable-ghost',
        forceFallback: true,
        fallbackOnBody: false,
        fallbackTolerance: 0,
        onEnd: function (evt) {
            var itemEl = evt.item;
            if (evt.newIndex === 0) {
                $(itemEl).find('input[name="time_after_prev"]')    .val(0);
                $(itemEl).find('input[name="distance_after_prev"]').val(0);
                $(itemEl).find('input[name="stop_time"]')          .val(0);
            }
            handleCities($);
        }
    });
}

function getNewCityForm($) {
    var nodeType = $('[name*="[0][node_type]"]').val();
    var selectCity = $('<select>', {
        'name': 'city'
    });
    Object.keys(citiesFullList).forEach(function(key) {
        if (citiesFullList[key].parent_id === 0) {
            $(selectCity).append('<option value="' + key + '">' + citiesFullList[key].name + '</option>');
        }
    });

    var selectStation = $('<select>', {
        'name': 'station'
    });

    switch (nodeType) {
        case 'tur':            
            var inputTimeAfterPrev = $('<div>', {
                'class': 'edit-time-extended'
            });
            var inputTimeAfterPrevDays = $('<input>', {
                'type': 'number',
                'min': 0,
                'value': 0,
                'name': 'time_after_prev_days'
            });
            var inputTimeAfterPrevHours = $('<input>', {
                'type': 'number',
                'min': 0,
                'value': 0,
                'name': 'time_after_prev_hours'
            });
            var inputTimeAfterPrevMinutes = $('<input>', {
                'type': 'number',
                'min': 0,
                'value': 0,
                'name': 'time_after_prev'
            });
            $(inputTimeAfterPrev).append(inputTimeAfterPrevDays);
            $(inputTimeAfterPrev).append($('<span>').text('дн'));
            $(inputTimeAfterPrev).append(inputTimeAfterPrevHours);
            $(inputTimeAfterPrev).append($('<span>').text('ч'));
            $(inputTimeAfterPrev).append(inputTimeAfterPrevMinutes);
            $(inputTimeAfterPrev).append($('<span>').text('мин'));
            break;
        default:            
            var inputTimeAfterPrev = $('<input>', {
                'type': 'number',
                'min': 0,
                'value': 0,
                'name': 'time_after_prev'
            });
            break;
    }

    var inputDistanceAfterPrev = $('<input>', {
        'class': nodeType === 'tur' ? 'input-shorter' : '',
        'type': 'number',
        'min': 0,
        'value': 0,
        'name': 'distance_after_prev',
        'step': 0.01
    });

    var inputPriceAfterPrev = $('<input>', {
        'class': nodeType === 'mezdugorodnii_reis' ? '' : 'hidden',
        'type': 'number',
        'min': 0,
        'value': 0,
        'name': 'price_after_prev',
        'step': 0.01
    });

    switch (nodeType) {
        case 'tur':            
            var inputStopTime = $('<div>', {
                'class': 'edit-time-extended'
            });
            var inputStopTimeDays = $('<input>', {
                'type': 'number',
                'min': 0,
                'value': 0,
                'name': 'stop_time_days'
            });
            var inputStopTimeHours = $('<input>', {
                'type': 'number',
                'min': 0,
                'value': 0,
                'name': 'stop_time_hours'
            });
            var inputStopTimeMinutes = $('<input>', {
                'type': 'number',
                'min': 0,
                'value': 0,
                'name': 'stop_time'
            });
            $(inputStopTime).append(inputStopTimeDays);
            $(inputStopTime).append($('<span>').text('дн'));
            $(inputStopTime).append(inputStopTimeHours);
            $(inputStopTime).append($('<span>').text('ч'));
            $(inputStopTime).append(inputStopTimeMinutes);
            $(inputStopTime).append($('<span>').text('мин'));
            break;
        default:            
            var inputStopTime = $('<input>', {
                'type': 'number',
                'min': 0,
                'value': 0,
                'name': 'stop_time'
            });
            break;
    }
    
    var btnRemoveCity = $('<input>', {
        'class': 'btn-remove-city btn-like-link',
        'type': 'button',
        'value': 'Убрать пункт'
    });

    var sortHandleMarker = $('<div>', {
        'class': 'sort-handle-marker'
    });
    
    var newCity = $('<li>', { 'class': 'city-form' });
    $(newCity).append(selectCity);
    $(newCity).append(selectStation);
    $(newCity).append(inputTimeAfterPrev);
    $(newCity).append(inputDistanceAfterPrev);
    $(newCity).append(inputPriceAfterPrev);
    $(newCity).append(inputStopTime);
    $(newCity).append(btnRemoveCity);
    $(newCity).append(sortHandleMarker);

    $(selectCity).change(function(){
        $(selectStation).html('');
        Object.keys(citiesFullList).forEach(function(key) {
            if (citiesFullList[key].parent_id.toString() === $(selectCity).val()) {
                $(selectStation).append('<option value="' + key + '">' + citiesFullList[key].name + '</option>');
            }
        });
        if ($(selectStation).html() === '') $(selectStation).append('<option value="-1">&mdash;</option>');
    });

    $(selectCity)            .change(function(){handleCities($);});
    $(selectStation)         .change(function(){handleCities($);});
    $(inputTimeAfterPrev)    .change(function(){handleCities($);});
    $(inputDistanceAfterPrev).change(function(){handleCities($);});
    $(inputPriceAfterPrev)   .change(function(){handleCities($);});
    $(inputStopTime)         .change(function(){handleCities($);});
    
    $(selectCity).trigger('change');

    $(btnRemoveCity).click(function(){
        $(newCity).remove();
        handleCities($);
    });

    return newCity;
}

function addLoadedCitiesRules($, loadedCities, citiesContainer) {
    for (var i = 0; i < loadedCities.length; i++) {
        var newCity = getNewCityForm($);

        var loadedCityLine = loadedCities[i].split(' ');
        
        var city              = loadedCityLine[0];
        var station           = loadedCityLine[1];
        var timeAfterPrev     = loadedCityLine[2];
        var distanceAfterPrev = loadedCityLine[3];
        var stopTime          = loadedCityLine[4];
        var priceAfterPrev    = loadedCityLine[5] ? loadedCityLine[5] : 0;

        $(newCity).find('select[name="city"]')              .val(city);
        $(newCity).find('select[name="city"]')              .trigger('change');
        $(newCity).find('select[name="station"]')           .val(station);
        setTimeAfterPrev($, newCity, timeAfterPrev);
        $(newCity).find('input[name="distance_after_prev"]').val(distanceAfterPrev);
        $(newCity).find('input[name="price_after_prev"]')   .val(priceAfterPrev);
        setStopTime($, newCity, stopTime);

        $(citiesContainer).append(newCity);
        $(newCity).find('select[name="station"]').trigger('change');
    }
}

function handleCities($) {
    $('[id*="0-route-route"]').val('');
    
    $('.city-form').each(function() {
        var city              = $(this).find('select[name="city"]').val();
        var station           = $(this).find('select[name="station"]').val();
        var timeAfterPrev     = getTimeAfterPrev($, $(this));
        var distanceAfterPrev = $(this).find('input[name="distance_after_prev"]').val();
        var priceAfterPrev    = $(this).find('input[name="price_after_prev"]').val();
        var stopTime          = getStopTime($, $(this));

        var temp = $('[id*="0-route-route"]').val();
        temp += city + ' ' + station + ' ' + timeAfterPrev + ' ' + distanceAfterPrev + ' ' + stopTime + ' ' + priceAfterPrev + '\n';

        $('[id*="0-route-route"]').val(temp);
    });

    var temp = $('[id*="0-route-route"]').val();
    $('[id*="0-route-route"]').val($.trim(temp));
}

function getTimeAfterPrev($, cityForm) {
    var nodeType = $('[name*="[0][node_type]"]').val();
    switch (nodeType) {
        case 'tur':
            var days    = parseInt($(cityForm).find('input[name="time_after_prev_days"]').val());
            var hours   = parseInt($(cityForm).find('input[name="time_after_prev_hours"]').val());
            var minutes = parseInt($(cityForm).find('input[name="time_after_prev"]').val());
            return days * 24 * 60 + hours * 60 + minutes;
        default: 
            return $(cityForm).find('input[name="time_after_prev"]').val();
    }
}

function getStopTime($, cityForm) {
    var nodeType = $('[name*="[0][node_type]"]').val();
    switch (nodeType) {
        case 'tur':
            var days    = parseInt($(cityForm).find('input[name="stop_time_days"]').val());
            var hours   = parseInt($(cityForm).find('input[name="stop_time_hours"]').val());
            var minutes = parseInt($(cityForm).find('input[name="stop_time"]').val());
            return days * 24 * 60 + hours * 60 + minutes;
        default: 
            return $(cityForm).find('input[name="stop_time"]').val();
    }
}

function setTimeAfterPrev($, cityForm, timeAfterPrev) {
    var nodeType = $('[name*="[0][node_type]"]').val();
    switch (nodeType) {
        case 'tur':
            var days      = Math.floor(timeAfterPrev / 1440);
            var remainder = timeAfterPrev % 1440;
            var hours     = Math.floor(remainder / 60);
            var minutes   = remainder % 60;
            $(cityForm).find('input[name="time_after_prev_days"]') .val(days);
            $(cityForm).find('input[name="time_after_prev_hours"]').val(hours);
            $(cityForm).find('input[name="time_after_prev"]')      .val(minutes);
            break;
        default: 
            $(cityForm).find('input[name="time_after_prev"]').val(timeAfterPrev);
    }
}

function setStopTime($, cityForm, stopTime) {
    var nodeType = $('[name*="[0][node_type]"]').val();
    switch (nodeType) {
        case 'tur':
            var days      = Math.floor(stopTime / 1440);
            var remainder = stopTime % 1440;
            var hours     = Math.floor(remainder / 60);
            var minutes   = remainder % 60;
            $(cityForm).find('input[name="stop_time_days"]') .val(days);
            $(cityForm).find('input[name="stop_time_hours"]').val(hours);
            $(cityForm).find('input[name="stop_time"]')      .val(minutes);
            break;
        default: 
            $(cityForm).find('input[name="stop_time"]').val(stopTime);
    }
}