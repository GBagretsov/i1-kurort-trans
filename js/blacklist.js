(function ($) {
  Drupal.behaviors.blacklistBehavior = {
    attach: function (context, settings) {
      $(context).find('#blacklist').once('blacklist').each(function() {
        initBlacklist($);
        handleAddButton($);
        handleSearchButton($);
      });
    }
  };
})(jQuery);

function initBlacklist($) {
    loadBlacklist();
    $('#btnReset').click(function() {
        $('#searchData').val('');
        loadBlacklist();
    });
}

function handleAddButton($) {
    $('#btnAddToBlacklist').click(function() {
        var addData = $('#addData').val().trim();
        if (addData === '') return;
        $.post('/api/blacklist/add', { data: addData }, function(data) {
            if (data.result === 'ok') {
                var li = getLiForBlacklistItem({
                    id: data.id,
                    data: data.data
                });
                if ($('#blacklist li').length === 0) $('#blacklist').empty();
                $('#blacklist').prepend(li);
                $(li).addClass('new-element');
            } else {
                alert('Ошибка! ' + data.description);
            }
        });
    });
}

function handleSearchButton($) {
    $('#btnSearchBlacklist').click(function() {
        var searchData = $('#searchData').val().trim();
        if (searchData === '') return;
        loadBlacklist(searchData);
    });
}

function loadBlacklist(searchData) {
    var $ = jQuery;
    var blacklist = $('#blacklist');
    $(blacklist).html('Загрузка...');
    $.get('/api/blacklist', { data: searchData }, function(data) {
        if (data.length === 0) {
            $(blacklist).html('Список пуст');
            return;
        }
        $(blacklist).empty();
        $.each(data, function(i, el) {
            var li = getLiForBlacklistItem(el);
            $(blacklist).append(li);
        });
    });
}

function getLiForBlacklistItem(item) {
    var li = $('<li>').text(item.data);
    var a = $('<a>', {
        'href': '/api/blacklist/remove/' + item.id
    }).text('Удалить');
    $(li).append(a);
    $(a).click(function(e) {
        e.preventDefault();
        $.post($(this).attr('href'), function(data) {
            if (data.result === 'ok') {
                $(a).remove();
                $(li).append(' &mdash; удалено');
            }
        });
    });
    return li;
}
