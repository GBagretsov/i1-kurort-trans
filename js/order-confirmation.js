(function ($) {
  Drupal.behaviors.confirmOrder = {
    attach: function (context, settings) {
      $(context).find('.order-confirm').once('confirm-order').each(function() {
        handleConfirm($(this));
      });
    }
  };
})(jQuery);

function handleConfirm(confirm) {
    var $ = jQuery;

    var orderId = $(confirm).data('order-id');
    var orderHash = $(confirm).data('order-hash');
    var step1 = $(confirm).find('.step-1');
    var step2 = $(confirm).find('.step-2');
    var noSms = $(confirm).find('.no-sms');
    var smsError = $(confirm).find('.sms-error');
    $(noSms).hide();
    $(noSms).removeClass('hidden');
    $(smsError).hide();
    $(smsError).removeClass('hidden');

    var btnCheckCode     = $(step2).find('button.btn-primary');
    var btnSendAgain     = $(confirm).find('button.btnSendAgain');
    var btnChooseAnother = $(step2).find('button.btnChooseAnother');

    if ($(step1).length > 0) {
        $(step1).find('button').click(function () {
            var phoneIndex = $(step1).find('select').val();
            sendConfirmationCode(orderId, phoneIndex, orderHash);
            $(step1).fadeOut(200, function () {
                $(step2).removeClass('hidden');
                $(step2).hide();
                $(step2).fadeIn(200);
            });
        });
    } else {
        sendConfirmationCode(orderId, 0, orderHash);
    }

    $(btnCheckCode).click(function () {
        var code = $(step2).find('.confirm-code').val();
        var phoneIndex = $(step1).find('select').val() || 0;

        var deferred = $.Deferred();
        deferred
            .done(function (data) {
                $.post('/api/order-info/send/' + orderId + '/' + phoneIndex, {
                    hash: orderHash
                });
                $(step1).remove();
                $(step2).remove();
                $('#order-id-' + orderId + ' .panel-heading .status-text')
                    .addClass('text-success')
                    .removeClass('text-info')
                    .text('Оплата наличными при посадке');
                var successMessage =
                    $('<div class="alert alert-success fade" role="alert">' + data.description + '</div>');
                $(confirm).find('.alert').not('.sms-error').remove();
                $(confirm).append(successMessage);
                setTimeout(function () {
                    $(successMessage).addClass('in');
                }, 100);
            })
            .fail(function (data) {
                if (data.status === 0) {
                    $(step1).remove();
                    $(step2).remove();
                    $('#order-id-' + orderId + ' .panel-heading .status-text')
                        .addClass('text-danger')
                        .removeClass('text-info')
                        .text('Отменён');
                    $('#order-id-' + orderId + ' .panel-heading a').remove();
                }
                $(confirm).find('.alert').not('.sms-error').remove();
                var errorMessage =
                    $('<div class="alert alert-danger alert-dismissible fade" role="alert">' + data.description + '</div>');
                var closeBtn = $('\
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
                        <span aria-hidden="true">&times;</span>\
                    </button>\
                ');
                $(confirm).append(errorMessage);
                setTimeout(function () {
                    $(errorMessage).addClass('in');
                }, 100);
                if (data.status !== 0) {
                    $(errorMessage).prepend($(closeBtn));
                }
            })
            .always(function () {
                $(btnCheckCode).prop('disabled', false);
                $(btnSendAgain).prop('disabled', false);
                $(btnChooseAnother).prop('disabled', false);
            });

        $(this).prop('disabled', true);
        $(btnSendAgain).prop('disabled', true);
        $(btnChooseAnother).prop('disabled', true);

        checkConfirmationCode(orderId, code, orderHash, deferred);
    });

    $(btnSendAgain).click(function () {
        $(confirm).find('.alert').not('.sms-error').remove();
        $(noSms).hide();
        $(smsError).hide();
        var phoneIndex = $(step1).find('select').val() || 0;
        sendConfirmationCode(orderId, phoneIndex, orderHash);
    });

    $(btnChooseAnother).click(function () {
        $(confirm).find('.alert').not('.sms-error').remove();
        $(step2).fadeOut(200, function () {
            $(step1).fadeIn(200);
        });
    });

    function sendConfirmationCode(orderId, phoneIndex, hash) {
        var $ = jQuery;
        $.post('/api/order-confirm/send', {
            order_id: orderId,
            phone_index: phoneIndex,
            hash: hash
        }).done(function(data) {
            if (data.result === 'success' && data.sent_amount < 3) {
                setTimeout(function() {
                    $(noSms).show();
                }, 15000);
            }
        }).fail(function() {
            $(smsError).show();
        });
    }
}

function checkConfirmationCode(orderId, code, hash, deferred) {
    var $ = jQuery;
    $.post('/api/order-confirm/check', {
        order_id: orderId,
        code: code,
        hash: hash
    }).done(function(data) {
        if (data.result === 'success') {
            deferred.resolve(data);
        } else {
            deferred.reject(data);
        }
    }).fail(function () {
        deferred.reject({
            description: 'Произошла ошибка. Попробуйте повторить позднее.'
        });
    });
}
