(function ($) {
  Drupal.behaviors.citiesCoordsEdit = {
    attach: function (context, settings) {
      $(context).find('#field-koordinaty-values').once('cities-coords-edit').each(function() {
        ymaps.ready(editMapInit);
        $('#field-koordinaty-values').after($('<div>', {
            'id': 'map',
            'style': 'width: 600px; height: 400px'
        }));
      });
    }
  };
})(jQuery);

var cityMarker;

function editMapInit() {
    var $ = jQuery;

    var lat  = parseFloat($('#edit-field-koordinaty-0-value').val());
    var long = parseFloat($('#edit-field-koordinaty-1-value').val());

    var coords;
    if (isNaN(lat)) {
        coords = [53.41020011, 58.97706584];
    } else {
        coords = [lat, long];
    }

    var myMap = new ymaps.Map('map', {
        center: coords, 
        zoom: isNaN(lat) ? 11 : 18
    });

    if (!isNaN(lat)) {
        var cityName = $('#edit-name-0-value').val();
        cityMarker = new ymaps.Placemark(coords, {
            hintContent: cityName
        });
        myMap.geoObjects.add(cityMarker);
    }

    myMap.events.add('click', function (e) {
        var cityName = $('#edit-name-0-value').val();
        var coords = e.get('coords');
        if (cityMarker) {
            cityMarker.geometry.setCoordinates(coords);
            cityMarker.properties.set('hintContent', cityName);
        } else {
            cityMarker = new ymaps.Placemark(coords, {
                hintContent: cityName
            });
            myMap.geoObjects.add(cityMarker);
        }
        $('#edit-field-koordinaty-0-value').val(coords[0]);
        $('#edit-field-koordinaty-1-value').val(coords[1]);
    });
}