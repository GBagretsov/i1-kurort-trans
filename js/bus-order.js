(function ($) {
  Drupal.behaviors.busOrderBehavior = {
    attach: function (context, settings) {
      $(context).find('body').once('bus-order').each(function() {
        if ($('[id*="webform_submission_zakaz_avtobusa"]').length === 0) return;
        addOrderButtons($);
        addOrderButtonsHandlers($);
      });
    }
  };
})(jQuery);

function addOrderButtons($) {
    $('article.avtobus').each(function(i, el) {
        var busId = $(el).data('history-node-id');
        var button = $('<button>', {
            'class': 'btn-primary btn btn-bus-order',
            'data-bus-id': busId
        }).text('Заказать');
        $(el).append(button);
    });
}

function addOrderButtonsHandlers($) {
    $('.btn-bus-order').each(function(i, el) {
        $(el).click(function() {
            var busId = $(this).data('bus-id');
            $('#edit-model-avtobusa').val(busId);
            var form = $('[id*="webform_submission_zakaz_avtobusa"]');
            $('html, body').animate({
                scrollTop: $(form).offset().top - 245
            }, 500);
        });
    });
}