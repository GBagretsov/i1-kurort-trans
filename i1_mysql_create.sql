CREATE TABLE `routepattern` (
	`id` INT NOT NULL,
	`price` FLOAT NOT NULL,
	`tickets_amount` INT NOT NULL,
	`open_sale_before` INT NOT NULL,
	`date_rules` TEXT NOT NULL,
	`time_rules` TEXT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `routetimetable` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`route_pattern_id` INT NOT NULL,
	`route_date_time` DATETIME NOT NULL,
	`tickets_sold` INT NOT NULL,
	`status` TINYINT NOT NULL DEFAULT '1',
	PRIMARY KEY (`id`)
);

CREATE TABLE `routepattern_cities` (
	`route_pattern_id` INT NOT NULL,
	`city_id` INT NOT NULL,
	`station_id` INT,
	`city_order` INT NOT NULL,
	`time_after_prev` INT NOT NULL,
	`distance_after_prev` FLOAT NOT NULL,
	`price_after_prev` FLOAT NOT NULL DEFAULT '0',
	`stop_time` INT NOT NULL,
	PRIMARY KEY (`route_pattern_id`,`city_order`)
);

CREATE TABLE `routes_users` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`payment_id` VARCHAR(255) UNIQUE,
	`user_id` INT NOT NULL,
	`price` FLOAT NOT NULL,
	`routetimetable_id` INT NOT NULL,
	`from_id` INT NOT NULL,
	`to_id` INT NOT NULL,
	`status` INT NOT NULL,
	`pay_before` DATETIME,
	`sms_sent` BINARY NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
);

CREATE TABLE `blacklist` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`data` VARCHAR(255) NOT NULL UNIQUE,
	PRIMARY KEY (`id`)
);

CREATE TABLE `order_confirm` (
	`order_id` INT NOT NULL UNIQUE,
	`code` MEDIUMINT NOT NULL,
	`last_sent` DATETIME NOT NULL,
	`sent_amount` TINYINT NOT NULL DEFAULT 0,
	`confirm_attempts` TINYINT NOT NULL DEFAULT 0,
	PRIMARY KEY (`order_id`)
);

ALTER TABLE `routetimetable` ADD CONSTRAINT `routetimetable_fk0` FOREIGN KEY (`route_pattern_id`) REFERENCES `routepattern`(`id`) ON DELETE CASCADE;

ALTER TABLE `routepattern_cities` ADD CONSTRAINT `routepattern_cities_fk0` FOREIGN KEY (`route_pattern_id`) REFERENCES `routepattern`(`id`) ON DELETE CASCADE;

ALTER TABLE `routetimetable` ADD UNIQUE `routetimetable_unique` (`route_pattern_id`, `route_date_time`);

ALTER TABLE `routes_users` ADD CONSTRAINT `routes_users_fk0` FOREIGN KEY (`routetimetable_id`) REFERENCES `routetimetable`(`id`) ON DELETE CASCADE;

ALTER TABLE `order_confirm` ADD CONSTRAINT `order_confirm_fk0` FOREIGN KEY (`order_id`) REFERENCES `routes_users`(`id`) ON DELETE CASCADE;
