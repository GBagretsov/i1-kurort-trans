<?php

namespace Drupal\i1\Sms;

class ePochtaSmsSender implements SmsSenderInterface
{
    private $login;
    private $password;

    public function __construct()
    {
        include __DIR__.'/../../sms_credentials.php';
        $this->login = $credentials['login'];
        $this->password = $credentials['password'];
    }

    /**
     * @param string $number
     * @param string $text
     * @return void
     */
    public function sendSms($number, $text)
    {
        // TODO: проверять формат номера
        $number = substr($number, 1);
        $src = '<?xml version="1.0" encoding="UTF-8"?>
                <SMS>
                    <operations>
                    <operation>SEND</operation>
                    </operations>
                    <authentification>
                    <username>'.$this->login.'</username>
                    <password>'.$this->password.'</password>
                    </authentification>
                    <message>
                    <sender>KurortTrans</sender>
                    <text>'.$text.'</text>
                    </message>
                    <numbers>
                    <number>'.$number.'</number>
                    </numbers>
                </SMS>';

        $Curl = curl_init();
        $CurlOptions = array(
            CURLOPT_URL => 'http://api.atompark.com/members/sms/xml.php',
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_POST => true,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 15,
            CURLOPT_TIMEOUT => 100,
            CURLOPT_POSTFIELDS => array('XML' => $src),
        );
        curl_setopt_array($Curl, $CurlOptions);
        $Result = curl_exec($Curl);
        curl_close($Curl);
    }
}