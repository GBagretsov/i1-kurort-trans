<?php

namespace Drupal\i1\Sms;


interface SmsSenderInterface
{
    /**
     * @param string $number
     * @param string $text
     * @return void
     */
    public function sendSms($number, $text);
}