<?php

namespace Drupal\i1\Sms;

class SmsSender implements SmsSenderInterface
{
    /**
     * @var string
     */
    private $sms_service_name = 'ePochta';

    /**
     * @var SmsSenderInterface
     */
    private $sender;

    public function __construct()
    {
        switch ($this->sms_service_name) {
            case 'ePochta':
            default:
                $this->sender = new ePochtaSmsSender();
                break;
        }
    }

    /**
     * @param string $number
     * @param string $text
     * @return void
     */
    public function sendSms($number, $text)
    {
        $this->sender->sendSms($number, $text);
    }
}