<?php
 
namespace Drupal\i1\Controller;

use DateTime;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Query\Condition;
use Drupal\i1\RouteUtil;
use Drupal\i1\OrderUtil;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class StatsController extends ControllerBase {

    public function showStatsPage() {        
        return array(
            '#title' => 'Статистика',
            '#theme' => 'stats_page',
            '#cache' => ['max-age' => 0],
        );
    }

    public function getStats() {
        $response = array();

        $begin_date = \Drupal::request()->query->get('begin_date');
        $end_date   = \Drupal::request()->query->get('end_date');
        $type       = \Drupal::request()->query->get('type');
        $route_id   = \Drupal::request()->query->get('route_id');

        if ($route_id) {
            $nids = array($route_id);
        } else if ($type) {
            $nids = \Drupal::entityQuery('node')->condition('type', $type)->execute();
        } else {
            $nids = $this->getAllRoutePatternIDs();
        }

        $nodes = \Drupal\node\Entity\Node::loadMultiple($nids);
        $connection = \Drupal::database();
        $transaction = $connection->startTransaction();

        foreach ($nodes as $n) {
            $route_pattern_id = $n->id();
            $tickets_amount = $n->field_kolicestvo_biletov->value;
            $query = $connection->select('routetimetable', 'rt');
            if ($begin_date) $query->condition('route_date_time', date_format(new \DateTime($begin_date), 'Y-m-d H:i:s'), '>=');
            if ($end_date)   $query->condition('route_date_time', date_format(date_add(new \DateTime($end_date), date_interval_create_from_date_string('1 day')), 'Y-m-d H:i:s'), '<=');
            $query->condition('rt.route_pattern_id', $route_pattern_id);
            $query->fields('rt', array(
                'id',
                'route_date_time',
                'tickets_sold',
            ));
            $query->orderBy('rt.route_date_time', 'ASC');
            $timetable = $query->execute()->fetchAll();
            foreach ($timetable as $t) {
                $route = new \stdClass();
                $route->id = $t->id;
                $route->date_time = $t->route_date_time;
                $route->name = $n->getTitle();
                $route->tickets_amount = $tickets_amount;
                $route->tickets_sold  = $t->tickets_sold;

                $query = $connection->select('routes_users', 'ru');
                $query->condition('ru.routetimetable_id', $t->id);

                $status_condition = new Condition('OR');
                $status_condition->condition('status', 2);
                $status_condition->condition('status', 3);
                $status_condition->condition('status', 4);
                $query->condition($status_condition);

                $query->addExpression('ROUND(SUM(price), 2)', 'total');
                $query->groupBy('ru.routetimetable_id');

                $route->earnings = $query->execute()->fetchField();
                if (!$route->earnings) $route->earnings = 0;

                $response[] = $route;
                if (count($response) > 100) {
                    $response[] = 'break';
                    break 2;
                }
            }
        }

        return new JsonResponse($response);
    }

    public function showPassengersPage($id) {
        try {
            $connection = \Drupal::database();
            $query = $connection->select('routetimetable', 'rt');
            $query->condition('rt.id', $id);
            $query->fields('rt', array(
                'route_pattern_id',
                'route_date_time',
            ));
            $row = $query->execute()->fetch();
            if (!$row) throw new \Exception('route not found');

            $date_time = new DateTime($row->route_date_time);
            $node = \Drupal\node\Entity\Node::load($row->route_pattern_id);
            if (!$node) throw new \Exception('node not found');
            $title = $node->getTitle();

            return array(
                '#title' => $title.', '.$date_time->format('d-m-Y H:i'),
                '#theme' => 'passengers_page',
                '#cache' => ['max-age' => 0],
                '#orders' => OrderUtil::getOrdersByTimetableId($id),
            );
        } catch (\Exception $e) {
            throw new NotFoundHttpException();
        }
    }

    public function getRouteNames() {
        $response = array();

        $nids = $this->getAllRoutePatternIDs();
        $nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

        foreach ($nodes as $n) {
            $route_pattern = new \stdClass();
            $route_pattern->id = $n->id();
            $route_pattern->name = $n->getTitle();
            $route_pattern->type = $n->getType();
            $response[] = $route_pattern;
        }

        usort($response, array($this, 'sortByName'));

        \Drupal::service('page_cache_kill_switch')->trigger();
        return new JsonResponse($response);
    }

    private function getAllRoutePatternIDs()
    {
        return array_merge(
            \Drupal::entityQuery('node')->condition('type', 'mezdugorodnii_reis')->execute(),
            \Drupal::entityQuery('node')->condition('type', 'ekskursia')         ->execute(),
            \Drupal::entityQuery('node')->condition('type', 'tur')               ->execute()
        );
    }

    private function sortByName($a, $b)
    {
        if ($a->name == $b->name) {
            return 0;
        }
        return ($a->name < $b->name) ? -1 : 1;
    }
}
