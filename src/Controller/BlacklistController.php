<?php
 
namespace Drupal\i1\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
 
class BlacklistController extends ControllerBase {

    public function showBlacklistPage() {
        return array(
            '#title' => 'Чёрный список номеров',
            '#theme' => 'blacklist_page',
            '#cache' => ['max-age' => 0],
        );
    }

    public function getBlacklist() {
        $data = \Drupal::request()->query->get('data');
        $connection = \Drupal::database();
        $query = $connection->select('blacklist', 'bl');
        $query->condition('bl.data', '%'.$data.'%', 'LIKE');
        $query->fields('bl', array(
            'id',
            'data',
        ));
        $query->orderBy('bl.id', 'DESC');
        $blacklist = $query->execute()->fetchAll();
        return new JsonResponse($blacklist);
    }

    public function addToBlacklist() {
        $data = \Drupal::request()->request->get('data');
        if (!$data || $data === '') {
            return new JsonResponse([
                'result' => 'error',
                'description' => 'Отсутствуют данные',
            ]);
        }

        list($is_valid, $data) = i1_transform_phone($data);
        if (!$is_valid) {
            return new JsonResponse([
                'result' => 'error',
                'description' => 'Это не похоже на номер телефона',
            ]);
        }

        $connection = \Drupal::database();
        $query = $connection->insert('blacklist');
        $query->fields(['data' => $data]);

        try {
            $id = $query->execute();
        } catch (\Drupal\Core\Database\IntegrityConstraintViolationException $e) {
            return new JsonResponse([
                'result' => 'error',
                'description' => 'Данные уже находятся в чёрном списке',
            ]);
        }

        return new JsonResponse([
            'result' => 'ok',
            'id'     => $id,
            'data'   => $data,
        ]);
    }

    public function removeFromBlacklist($id) {
        $connection = \Drupal::database();
        $query = $connection->delete('blacklist');
        $query->condition('id', $id);
        $query->execute();

        return new JsonResponse([
            'result' => 'ok',
            'id'     => $id,
        ]);
    }

    public static function isBlacklisted($data) {
        $connection = \Drupal::database();
        $query = $connection->select('blacklist', 'bl');
        $query->condition('bl.data', '%'.$data.'%', 'LIKE');
        $query->fields('bl', array(
            'id'
        ));
        $blacklist = $query->execute()->fetchAll();
        return count($blacklist) > 0;
    }

}
