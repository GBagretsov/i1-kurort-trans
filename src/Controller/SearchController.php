<?php
 
namespace Drupal\i1\Controller;

use \DateTime;
use Drupal\i1\RouteUtil;
use Drupal\Core\Controller\ControllerBase;
 
class SearchController extends ControllerBase {

    public function getRoutesSearchResults($from, $to, $date) {
        $cities_list = RouteUtil::getCitiesList('mezdugorodnii_reis');
        $suitable_routes = $this->getSuitableRoutes($from, $to, $date);

        foreach ($suitable_routes as $item) {
            $price_parts = explode('.', $item->price);
            $item->price_rub = $price_parts[0];
            $item->price_kop = count($price_parts) > 1 ? $price_parts[1] : '00';
            $item->travel_time_hours   = (int)$item->travel_time->format('%h');
            $item->travel_time_minutes = (int)$item->travel_time->format('%i');
            if (strlen($item->price_kop) === 1) $item->price_kop = $item->price_kop.'0';
            foreach ($item->route as $r) {
                $r->city_name      = isset($cities_list[$r->city_id]) ? $cities_list[$r->city_id]->name : 'Город, которого нет';
                $station_id = $r->station_id ? $r->station_id : -1;
                $r->station_name   = isset($cities_list[$station_id]) ? $cities_list[$station_id]->name : 'Вокзал, которого нет';
                $r->arrival_date   = RouteUtil::formatDate($r->arrival_date_time);
                $r->departure_date = RouteUtil::formatDate($r->departure_date_time);
                $r->arrival_time   = $r->arrival_date_time->format('H:i');
                $r->departure_time = $r->departure_date_time->format('H:i');
            }
        }

        \Drupal::service('page_cache_kill_switch')->trigger();
        
        return array(
            '#title' => 'Результаты поиска',
            '#theme' => 'routes_search_result',
            '#result' => $suitable_routes,
            '#from' => $from,
            '#to' => $to,
            '#cache' => ['max-age' => 0],
        );
    }

    private function getSuitableRoutes($from, $to, $date) {
        $suitable_routes = array();

        $min_departure_time = new DateTime('now');
        $min_departure_time->modify('+30 minutes');

        $connection = \Drupal::database();
        $query = $connection->select('routepattern', 'rp');
        $query->fields('rp', array('id', 'price', 'open_sale_before', 'tickets_amount'));
        $result = $query->execute()->fetchAll();

        foreach ($result as $row) {
            $node = entity_load('node', $row->id);            
            if (!$node->isPublished()) continue;
            $query = $connection->select('routepattern_cities', 'rpc');
            $query->fields('rpc', array('city_id', 'station_id', 'time_after_prev', 'stop_time'));
            $query->condition('rpc.route_pattern_id', $row->id);
            $query->orderBy('rpc.city_order');
            $row->route = $query->execute()->fetchAll();
            list($suitable, $from_index, $to_index) = $this->isRouteSuitable($row->route, $from, $to);
            if (!$suitable) continue;

            $query = $connection->select('routetimetable', 'rt');
            $query->condition('rt.route_pattern_id', $row->id);
            $query->condition('rt.route_date_time', $date.'%', 'LIKE');
            $query->addField('rt', 'id');
            $query->addField('rt', 'route_date_time');
            $query->addField('rt', 'status');
            $query->addField('rt', 'tickets_sold');
            $timetable = $query->execute()->fetchAll();

            foreach ($timetable as $t) {
                if (intval($t->status) !== 1) continue;
                $tickets_left = $row->tickets_amount - $t->tickets_sold;
                if ($tickets_left <= 0) continue;
                $start_date_time = new DateTime($t->route_date_time);
                $row->route[0]->arrival_date_time = clone $start_date_time;
                $row->route[0]->departure_date_time = clone $start_date_time;
                for ($i = 1; $i < count($row->route); $i++) {
                    $previous_city_id = $row->route[$i - 1]->city_id;
                    $current_city_id  = $row->route[$i]->city_id;
                    // Рассчитываем время прибытия
                    $row->route[$i]->arrival_date_time = clone $row->route[$i-1]->departure_date_time;
                    $minutes = intval($row->route[$i]->time_after_prev);
                    date_add($row->route[$i]->arrival_date_time, date_interval_create_from_date_string($minutes.' minutes'));
                    RouteUtil::changeDateTimeZone($row->route[$i]->arrival_date_time, $previous_city_id, $current_city_id);
                    // Рассчитываем время отправления
                    $row->route[$i]->departure_date_time = clone $row->route[$i]->arrival_date_time;
                    $minutes = intval($row->route[$i]->stop_time);
                    date_add($row->route[$i]->departure_date_time, date_interval_create_from_date_string($minutes.' minutes'));
                }
                $new = array();
                foreach ($row->route as $k => $v) {
                    if ($k >= $from_index && $k <= $to_index) $new[] = clone $v;
                }
                $new[0]->arrival_date_time = $new[0]->departure_date_time; // Симуляция начального пункта маршрута
                $new[0]->stop_time = 0; // Симуляция начального пункта маршрута

                if ($min_departure_time > $new[0]->departure_date_time) continue;

                $item = new \stdClass();
                $item->id = $t->id;
                $item->title = $node->get('field_nazvanie_dla_passazirov')->getValue()[0]['value'];
                $item->description = $node->get('body')->getValue() ? $node->get('body')->getValue()[0]['value'] : '';
                $item->additional  = $node->get('field_procie_uslovia')->value;
                $item->price = RouteUtil::getSubRoutePrice($row->id, $from, $to);
                if (floatval($item->price) === floatval(0)) continue;
                $item->route = $new;
                $item->tickets_left = $tickets_left;

                // Время в пути с учётом разницы во времени
                $end_date_time = clone end($new)->arrival_date_time;
                $departure_city_id = $new[0]->city_id;
                $arrival_city_id = end($new)->city_id;
                RouteUtil::changeDateTimeZone($end_date_time, $arrival_city_id, $departure_city_id);
                $item->travel_time = date_diff($new[0]->departure_date_time, $end_date_time, TRUE);
                $suitable_routes[] = $item;
            }
        }

        usort($suitable_routes, 'self::sortByTime');
        return $suitable_routes;
    }

    private function isRouteSuitable($route, $from, $to) {
        $from_index = -1;
        $to_index = -1;

        for ($i = 0; $i < count($route); $i++) { 
            if ($from === $route[$i]->city_id || $from === $route[$i]->station_id) $from_index = $from_index === -1 ? $i : $from_index;
            if ($to === $route[$i]->city_id   || $to === $route[$i]->station_id)   $to_index = $i;
        }

        if ($from_index === -1 || $to_index === -1) return array(false, $from_index, $to_index); // Маршрут не проходит через город
        return array($from_index < $to_index, $from_index, $to_index);
    }

    private static function sortByTime($a, $b)
    {
        if ($a->route[0]->arrival_date_time == $b->route[0]->arrival_date_time) {
            return 0;
        }
        return ($a->route[0]->arrival_date_time < $b->route[0]->arrival_date_time) ? -1 : 1;
    }
}