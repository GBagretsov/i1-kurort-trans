<?php
 
namespace Drupal\i1\Controller;

use Drupal\i1\RouteUtil;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
 
class OpenedRoutesEditController extends ControllerBase {

    public function getOpenedRoutesInfo($node_id) {
        $sale_opened = RouteUtil::getRouteTimetableById($node_id);
        $build = [
            '#theme' => 'sale_opened_list',
            '#result' => $sale_opened,
        ];
        $markup = \Drupal::service('renderer')->renderPlain($build);
        $response = array();
        $response['result'] = $markup;
        return new JsonResponse($response);
    }

    public function openRoute($id) {
        $response = array();
        $connection = \Drupal::database();
        $transaction = $connection->startTransaction();
        
        $query = $connection->update('routetimetable');
        $query->fields(['status' => 1]);
        $query->condition('id', $id);
        $query->execute();
        
        $query = $connection->select('routetimetable', 'rt');
        $query->condition('rt.id', $id);
        $query->addField('rt', 'tickets_sold');
        $tickets_sold = $query->execute()->fetchField();

        $response['result'] = 'ok';
        $response['id'] = $id;
        $response['tickets_sold'] = $tickets_sold;
        return new JsonResponse($response);
    }

    public function cancelRoute($id) {
        $response = array();
        $connection = \Drupal::database();
        $transaction = $connection->startTransaction();
        
        $query = $connection->select('routetimetable', 'rt');
        $query->condition('rt.id', $id);
        $query->addField('rt', 'tickets_sold');
        $tickets_sold = $query->execute()->fetchField();
        $error = $tickets_sold > 0;

        if (!$error) {
            $query = $connection->update('routetimetable');
            $query->fields(['status' => 0]);
            $query->condition('id', $id);
            $query->execute();
        }

        $response['result'] = $error ? 'error' : 'ok';
        $response['id'] = $id;
        $response['tickets_sold'] = $tickets_sold;
        return new JsonResponse($response);
    }
}