<?php
 
namespace Drupal\i1\Controller;

use Drupal\Core\Controller\ControllerBase;
use \Drupal\i1\RouteUtil;
use \Drupal\i1\OrderUtil;
use \Drupal\i1\Controller\BlacklistController;
use Drupal\i1\Sms\SmsSender;
use \Drupal\user\Entity\User;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use YandexCheckout\Client;
use YandexCheckout\Model\Notification\NotificationWaitingForCapture;
 
class PurchaseController extends ControllerBase {

    public function showPurchaseForm($id, $from_id, $to_id, $prev_phones = NULL) {

        \Drupal::service('page_cache_kill_switch')->trigger();

        list($tickets_available, $tickets_sold) = $this->ticketsAvailable($id, \Drupal::database());
        if (!$tickets_available) {
            return array(
                '#title' => 'Заказ билета',
                '#markup' => 'На этот рейс билетов нет',
                '#cache' => ['max-age' => 0],
            );
        }

        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $phone = $user->getUsername();
        $route = RouteUtil::getSubRoute($id, $from_id, $to_id);

        $connection = \Drupal::database();
        $query = $connection->select('routetimetable', 'rt');
        $query->condition('rt.id', $id);
        $query->addField('rt', 'route_pattern_id');
        $route_pattern_id = $query->execute()->fetchField();

        $node = entity_load('node', $route_pattern_id);
        $route_type = $node->getType();
        $max_passengers = min(5, $node->field_kolicestvo_biletov->value - $tickets_sold);

        $real_from_id = $from_id !== '0' ? $from_id : $route[0]->city_id;
        $real_to_id   = $to_id   !== '0' ? $to_id   : $route[count($route) - 1]->city_id;
        
        return array(
            '#title' => 'Заказ билета',
            '#theme' => 'purchase_form',
            '#cache' => ['max-age' => 0],
            '#user' => [
                'id' => \Drupal::currentUser()->id(),
                'phone' => $phone,
            ],
            '#route' => [
                'id'      => $id,
                'from_id' => $real_from_id,
                'to_id'   => $real_to_id,
                'type'    => $route_type,
                'route'   => RouteUtil::getSubRoute($id, $from_id, $to_id),
                'max_passengers' => $max_passengers,
            ],
            '#prev_phones' => $prev_phones,
        );
    }

    public function bookPurchase() {

        $generic_error = array(
            '#title' => 'Ошибка',
            '#markup' => 'При заказе билета произошла ошибка',
            '#cache' => ['max-age' => 0],
        );

        $route_id = \Drupal::request()->request->get('route_id');
        $user_id  = intval(\Drupal::currentUser()->id());
        $email    = \Drupal::request()->request->get('email');

        $from_id  = \Drupal::request()->request->get('from_id');
        $to_id    = \Drupal::request()->request->get('to_id');

        $phones   = \Drupal::request()->request->get('phones');
        $all_phones_valid = TRUE;
        $transformed_phones = [];

        if (count($phones) > 5) return $generic_error;

        foreach ($phones as $phone) {
            list($is_valid, $transformed_phone) = i1_transform_phone($phone);
            if (!$is_valid || empty($transformed_phone)) {
                $all_phones_valid = FALSE;
            } else if (BlacklistController::isBlacklisted($transformed_phone)) {
                return $generic_error;
            }
            $transformed_phones[] = [
                'phone' => $transformed_phone,
                'is_valid' => $is_valid && !empty($transformed_phone),
            ];
        }

        if (!$all_phones_valid) return $this->showPurchaseForm($route_id, $from_id, $to_id, $transformed_phones);

        $connection = \Drupal::database();
        $transaction = $connection->startTransaction();
        
        $transformed_phones = array_map(function($a) { return $a['phone']; }, $transformed_phones);
        
        // На каждый телефон не более двух одновременных заказов со статусом "Ожидается оплата" или "Оплата наличными"
        foreach ($transformed_phones as $tp) {
            $query = $connection->query(
               'SELECT COUNT(*) as c
                FROM routes_users
                WHERE `personal_data` LIKE :pattern
                AND (status = 1 OR status = 4)',
                [
                    ':pattern' => '%'.$tp.'%',
                ]
            );
            $result = intval($query->fetchAll()[0]->c);
            if ($result > 1) {
                return $generic_error;
            }
        }        
        
        $payment_type = \Drupal::request()->request->get('payment_type');

        $booking_id = -1;
        $booking_amount = count($phones);
        $tickets_available = FALSE;
        
        try {
            list($tickets_available, $tickets_sold) = $this->ticketsAvailable($route_id, $connection, $booking_amount);

            if ($tickets_available) {
                $connection->update('routetimetable')
                    ->fields([
                        'tickets_sold' => $tickets_sold + $booking_amount,
                    ])
                    ->condition('id', $route_id)
                    ->execute();

                $query = $connection->select('routetimetable', 'rt');
                $query->condition('rt.id', $route_id);
                $query->addField('rt', 'route_pattern_id');
                $route_pattern_id = $query->execute()->fetchField();

                $node = entity_load('node', $route_pattern_id);
                $route_name = $node->get('field_nazvanie_dla_passazirov')->value;
                $route_type = $node->getType();

                $query = $connection->select('routepattern', 'rp');
                $query->condition('rp.id', $route_pattern_id);
                $query->addField('rp', 'price');
                $price = floatval($query->execute()->fetchField());
                if ($price === floatval(-1)) $price = RouteUtil::getSubRoutePrice($route_pattern_id, $from_id, $to_id);
                if ($price === floatval(0)) throw new \Exception('price cannot be equal to zero');

                $pay_before = new \DateTime('now');
                $pay_before->add(new \DateInterval('PT30M'));
                $pay_before = date_format($pay_before, 'Y-m-d H:i:s');

                if ($payment_type === 'cash') {
                    if (intval($user_id) === 0) { // Для анонимов подтверждение оплаты наличными
                        $status = 5;
                    } else {
                        $status = 4; // Для зарегистрированных подтверждение не требуется
                    }
                } else {
                    $status = 1; // При оплате картой подтверждение не требуется
                }

                // TODO: данные пассажиров
                $booking_id = $connection->insert('routes_users')
                    ->fields([
                        'routetimetable_id' => $route_id,
                        'user_id'           => $user_id,
                        'status'            => $status,
                        'from_id'           => $from_id,
                        'to_id'             => $to_id,
                        'price'             => $price * $booking_amount,
                        'personal_data'     => implode("\n", $transformed_phones),
                        'pay_before'        => $pay_before,
                    ])
                    ->execute();

                if ($status === 5) {
                    $code = random_int(10000, 99999);
                    $connection->insert('order_confirm')
                        ->fields([
                            'order_id'  => $booking_id,
                            'code'      => $code,
                            'last_sent' => date_format(new \DateTime('yesterday'), 'Y-m-d H:i:s'),
                        ])
                        ->execute();
                }

                $order_hash = $this->getOrderHash(OrderUtil::getOrderById($booking_id));
                setcookie('_kurort_o_'.$booking_id, $order_hash, time() + 60 * 15, '', '', FALSE, TRUE);
            }

        }
        catch (\Exception $e) {
            $transaction->rollBack();
            return $generic_error;
        }

        \Drupal::service('page_cache_kill_switch')->trigger();

        if (!$tickets_available) {
            return $generic_error;
        }

        if ($status === 4) {
            $this->sendConfirmationEmail(intval($user_id));
        }
        
        return array(
            '#title' => 'Билеты забронированы',
            '#theme' => 'booked_purchase_form',
            '#cache' => ['max-age' => 0],
            '#user' => [
                'id' => $user_id,
                'email' => user_load($user_id)->getEmail(),
            ],
            '#route' => [
                'id'      => $route_id,
                'from_id' => $from_id,
                'to_id'   => $to_id,
                'name' => $route_name,
                'type' => $route_type,
                'route'   => RouteUtil::getSubRoute($route_id, $from_id, $to_id),
            ],
            '#book_info' => [
                'phones' => $transformed_phones,
                'id' => $booking_id,
                'price' => $price * $booking_amount,
                'payment_type' => $payment_type,
                'hash' => $order_hash,
            ],
        );
    }

    public function purchase() {
        $order_id = \Drupal::request()->request->get('booking_id');

        $connection = \Drupal::database();
        $query = $connection->select('routes_users', 'ru');
        $query->condition('ru.id', $order_id);
        $query->addField('ru', 'price');
        $price = floatval($query->execute()->fetchField());

        $order = OrderUtil::getOrderById($order_id);
        $hash = $this->getOrderHash($order);

        $return_url = \Drupal::request()->getSchemeAndHttpHost().'/return?order_id='.$order_id.'&hash='.$hash;

        $client = $this->getYandexCheckoutClient();
        $payment = $client->createPayment(
            array(
                'amount' => array(
                    'value' => $price,
                    'currency' => 'RUB'
                ),
                'confirmation' => array(
                    'type' => 'redirect',
                    'return_url' => $return_url,
                ),
            ),
            $order_id
        );

        $payment_id = $payment->getId();
        $payment_url = $payment->getConfirmation()->getConfirmationUrl();

        $transaction = $connection->startTransaction();

        try {
            $connection->update('routes_users')
                ->fields([
                    'payment_id' => $payment_id,
                ])
                ->condition('id', $order_id)
                ->execute();
        }
        catch (\Exception $e) {
            $transaction->rollBack();
            return array(
                '#title' => 'Ошибка',
                '#markup' => 'При заказе билета произошла ошибка',
                '#cache' => ['max-age' => 0],
            );
        }
        
        \Drupal::service('page_cache_kill_switch')->trigger();
        return new TrustedRedirectResponse($payment_url);
    }

    public function purchaseReturn() {
        $order_id = \Drupal::request()->query->get('order_id');
        $hash = \Drupal::request()->query->get('hash');

        if (!$order_id || !$hash) {
            throw new NotFoundHttpException();
        }

        $order = OrderUtil::getOrderById($order_id);
        $real_hash = $this->getOrderHash($order);
        $order->hash = $real_hash;
        $order->deferred = TRUE;

        if ($real_hash !== $hash) {
            throw new NotFoundHttpException();
        }

        \Drupal::service('page_cache_kill_switch')->trigger();
        
        return array(
            '#title' => 'Проверяется оплата заказа...',
            '#theme' => 'purchase_success',
            '#anonymous' => \Drupal::currentUser()->isAnonymous(),
            '#cache' => ['max-age' => 0],
            '#order' => $order,
        );
    }

    public function purchaseError() {

        \Drupal::service('page_cache_kill_switch')->trigger();
        
        return array(
            '#title' => 'При оплате заказа произошла ошибка',
            '#theme' => 'purchase_error',
            '#cache' => ['max-age' => 0],
        );
    }

    public function confirmPayment() {
        $json = file_get_contents("php://input");

        if (!$json) throw new NotFoundHttpException();        

        $notification = new NotificationWaitingForCapture(json_decode($json, true));
        $payment = $notification->getObject();
        $client = $this->getYandexCheckoutClient();
        $now = new \DateTime('now');

        $connection = \Drupal::database();
        $transaction = $connection->startTransaction();

        // Проверяем статус заказа и вовремя ли пришла оплата

        $query = $connection->select('routes_users', 'ru');
        $query->condition('ru.payment_id', $payment->getId());
        $query->fields('ru', ['id', 'routetimetable_id', 'pay_before', 'status', 'user_id']);
        $row = $query->execute()->fetchAll()[0];
        $order_status = intval($row->status);
        $pay_before   = new \DateTime($row->pay_before); 
        $paid_in_time = $pay_before > $now; 

        // Проверяем статус рейса

        $query = $connection->select('routetimetable', 'rt');
        $query->condition('rt.id', $row->routetimetable_id);
        $query->addField('rt', 'status');
        $route_status = intval($query->execute()->fetchField());

        if ($route_status !== 1 || $order_status !== 1 || !$paid_in_time) {
            $client->cancelPayment(
                $payment->id,
                uniqid('', true)
            );
        } else {
            try {
                $connection->update('routes_users')
                    ->fields([
                        'status' => 2,
                    ])
                    ->condition('payment_id', $payment->getId())
                    ->execute();
                $user_id = intval($row->user_id);
                $this->sendConfirmationEmail($user_id);
                $client->capturePayment(
                    array(
                        'amount' => $payment->amount,
                    ),
                    $payment->id,
                    uniqid('', true)
                );
            }
            catch (\Exception $e) {
                $transaction->rollBack();
            }
        }
        
        return array(
            '#title' => 'Подтверждение платежа',
            '#markup' => '',
        );
    }

    private function ticketsAvailable($route_id, $connection, $amount = 1)
    {
        $query = $connection->select('routetimetable', 'rt');
        $query->condition('rt.id', $route_id);
        $query->addField('rt', 'route_pattern_id');
        $query->addField('rt', 'tickets_sold');
        $query->addField('rt', 'status');
        $rows = $query->execute()->fetchAll();
        if (count($rows) === 0) throw new NotFoundHttpException();
        $route = $rows[0];

        if (intval($route->status) !== 1) return array(FALSE, 0);

        $query = $connection->select('routepattern', 'rp');
        $query->addField('rp', 'tickets_amount');
        $query->condition('id', $route->route_pattern_id);
        $route_pattern = $query->execute()->fetchAll()[0];

        return array(
            $route_pattern->tickets_amount - $route->tickets_sold >= $amount,
            $route->tickets_sold,
        );
    }

    private function getYandexCheckoutClient() {
        include __DIR__.'/../../yandex_checkout_credentials.php';
        $client = new Client();
        $client->setAuth($credentials['shop_id'], $credentials['password']);
        return $client;
    }

    private function sendConfirmationEmail($user_id)
    {
        if ($user_id === 0) return;
        $to = user_load($user_id)->getEmail();

        $subject  = "Заказ билетов"; 

        $message  = '<p>Вы сделали заказ билетов на сайте Курорт-Транс</p>';
        $message .= '<p><a href="'.\Drupal::request()->getSchemeAndHttpHost().'/user">Перейти в личный кабинет</a></p>';

        $headers  = "Content-type: text/html; charset=utf-8 \r\n"; 

        mail($to, $subject, $message, $headers); 
    }

    public function sendOrderInfoSms($order_id, $number_index = 0)
    {
        try {
            $order = OrderUtil::getOrderById($order_id);
            if ($order->sms_sent === '1') {
                throw new \Exception('sms already sent');
            }

            $hash = \Drupal::request()->request->get('hash');
            $real_hash = $this->getOrderHash($order);
            if ($real_hash !== $hash) {
                throw new \Exception('invalid hash');
            }

            $sender = new SmsSender();
            $name = $order->route_name;
            $date_time = RouteUtil::formatDateForSmsOrderInfo($order->route[0]->departure_date_time);
            $text = 'Ваш рейс: '.$name.', '.$date_time.'. Заказ '.$order_id;
            $phone = $order->phones[$number_index];
            $sender->sendSms($phone, $text);

            $connection  = \Drupal::database();
            $query = $connection->update('routes_users');
            $query->fields([
                'sms_sent' => 1,
            ]);
            $query->condition('id', $order_id);
            $query->execute();

            return new JsonResponse([
                'status' => 'success',
            ]);
        } catch (\Exception $e) {
            return new JsonResponse([
                'status' => 'error',
            ]);
        }
    }

    public function getAdditionalStationInfo($station_id) {
        $response = array();
        $term = taxonomy_term_load($station_id);

        if (is_null($term)) return new JsonResponse($response);

        if ($term->hasField('field_koordinaty')) {
            if (!$term->get('field_koordinaty')->isEmpty()) {
                $coords = $term->get('field_koordinaty')->getValue();
                $response['latitude']  = $coords[0]['value'];
                $response['longitude'] = $coords[1]['value'];
            }
        }

        if ($term->hasField('field_fotografia')) {
            if (!$term->get('field_fotografia')->isEmpty()) {
                $response['photo'] = file_create_url($term->get('field_fotografia')->entity->getFileUri());
            }
        }

        if ($term->hasField('description')) {
            if (!$term->get('description')->isEmpty()) {
                $response['description'] = $term->description->value;
            }
        }

        return new JsonResponse($response);
    }

    public function showOrderInfoForm() {
        if (\Drupal::currentUser()->isAuthenticated()) {
            return new TrustedRedirectResponse(\Drupal::request()->getSchemeAndHttpHost().'/user');
        }
        return array(
            '#title' => 'Найдите ваш заказ',
            '#theme' => 'order_info_form',
        );
    }

    public function getOrderInfo() {
        \Drupal::service('page_cache_kill_switch')->trigger();

        list($is_valid, $phone) = i1_transform_phone(\Drupal::request()->request->get('phone'));
        $date = \Drupal::request()->request->get('date');
        $orders = $is_valid ? OrderUtil::getOrdersByPhoneAndDate($phone, $date) : [];

        foreach ($orders as $order) {
            $order_hash = $this->getOrderHash($order);
            $order->hash = $order_hash;
            setcookie('_kurort_o_'.$order->id, $order_hash, time() + 60 * 15, '', '', FALSE, TRUE);
        }
        
        return array(
            '#title' => 'Информация о заказе',
            '#theme' => 'order_info',
            '#cache' => ['max-age' => 0],
            '#orders'   => $orders,
            '#is_valid' => $is_valid,
            '#phone'    => $phone,
        );
    }

    private function getOrderHash($order)
    {
        $array = array(
            $order->id,
            $order->user_id,
            $order->price,
            $order->routetimetable_id,
            $order->from_id,
            $order->to_id,
            $order->route[0]->arrival_date_time->format('YmdHi'),
            implode(';', $order->phones),
            $order->pay_before,
        );
        $value = implode(';', $array);
        return hash('sha512', $value);
    }

    public function showOrderCancelConfirmForm($id) {
        $order = OrderUtil::getOrderById($id);
        if (!$this->checkIfCancelable($order)) throw new NotFoundHttpException();
        $order->hide_controls = TRUE;
        return array(
            '#title' => 'Вы уверены, что хотите отменить заказ?',
            '#theme' => 'cancel_order_form',
            '#cache' => ['max-age' => 0],
            '#order' => $order,
        );
    }

    public function cancelOrder($id) {
        $connection = \Drupal::database();
        $transaction = $connection->startTransaction();
        $order = OrderUtil::getOrderById($id);
        try {
            if (!$this->checkIfCancelable($order)) throw new NotFoundHttpException();
            
            // Уменьшаем количество проданных билетов
            $routetimetable_id = $order->routetimetable_id;
            $tickets_amount = count($order->phones);
            $query = $connection->query(
               'UPDATE routetimetable
                SET tickets_sold = tickets_sold - :tickets_amount
                WHERE id = :routetimetable_id',
                [
                    ':tickets_amount'    => $tickets_amount,
                    ':routetimetable_id' => $routetimetable_id,
                ]
            );

            // Меняем статус на "отменён"
            $query = $connection->update('routes_users');
            $query->condition('id', $order->id);
            $query->fields(['status' => 0]);
            $query->execute();

            // Если была произведена оплата картой, отменяем платёж
            if ($order->status === 2) {
                $client = $this->getYandexCheckoutClient();
                $client->createRefund(
                    array(
                        'amount' => array(
                            'value' => floatval($order->price),
                            'currency' => 'RUB',
                        ),
                        'payment_id' => $order->payment_id,
                    ),
                    uniqid('', true)
                );
            }

        } catch (NotFoundHttpException $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Exception $e) {
            $transaction->rollBack();
            return array(
                '#title' => 'Ошибка',
                '#markup' => 'Произошла ошибка',
                '#cache' => ['max-age' => 0],
            );
        }
        return array(
            '#title' => 'Заказ отменён',
            '#markup' => 'Ваш заказ отменён',
            '#cache' => ['max-age' => 0],
        );
    }

    private function checkIfCancelable($order) {
        if (!$order) return FALSE;

        // Проверяем, не выполнен ли заказ и не началась ли поездка (отмена за 30 минут)
        $cancel_before = clone $order->route[0]->arrival_date_time;
        $cancel_before->sub(new \DateInterval('PT30M'));
        if ($order->status === 0 || $order->status === 3 || $cancel_before < new \DateTime('now')) return FALSE;

        // Проверяем куки заказа
        if (isset($_COOKIE['_kurort_o_'.$order->id]) && $_COOKIE['_kurort_o_'.$order->id] === $this->getOrderHash($order)) return TRUE;

        // Проверяем user.id
        return \Drupal::currentUser()->isAuthenticated() && intval(\Drupal::currentUser()->id()) === $order->user_id;
    }

    public function sendOrderConfirmCode() {
        $response = array();
        $order_id    = intval(\Drupal::request()->request->get('order_id'));
        $phone_index = intval(\Drupal::request()->request->get('phone_index'));
        $hash        = \Drupal::request()->request->get('hash');

        $connection  = \Drupal::database();
        $transaction = $connection->startTransaction();

        try {
            $order = OrderUtil::getOrderById($order_id);
            $real_hash = $this->getOrderHash($order);
            if ($real_hash !== $hash) throw new \Exception('invalid hash');

            $query = $connection->select('order_confirm', 'oc');
            $query->fields('oc', ['code', 'last_sent', 'sent_amount']);
            $query->condition('order_id', $order_id);
            $row = $query->execute()->fetch();

            if (!$row) {
                throw new \Exception('row not found');
            }

            $last_sent = (new \DateTime($row->last_sent))->getTimestamp();
            $diff = abs(time() - $last_sent);
            $sent_amount = intval($row->sent_amount);

            // Проверяем время последней отправки СМС и количество отосланных СМС
            if ($diff < 15 || $sent_amount >= 3) {
                throw new \Exception();
            }

            $query = $connection->update('order_confirm');
            $query->fields([
                'sent_amount' => $sent_amount + 1,
                'last_sent'   => date_format(new \DateTime(), 'Y-m-d H:i:s'),
            ]);
            $query->condition('order_id', $order_id);
            $query->execute();

            $phone = $order->phones[$phone_index];
            $text = 'Код '.$row->code;
            $sender = new SmsSender();
            $sender->sendSms($phone, $text);

            $response['result'] = 'success';
            $response['description'] = 'Код подтверждения отправлен';
            $response['sent_amount'] = $sent_amount + 1;

        } catch (\Exception $e) {
            $transaction->rollBack();
            $response['result'] = 'error';
            $response['description'] = 'Произошла ошибка. Попробуйте повторить позднее.';
        }
        return new JsonResponse($response);
    }

    public function checkOrderConfirmCode() {
        $response = array();
        $order_id = intval(\Drupal::request()->request->get('order_id'));
        $code     = \Drupal::request()->request->get('code');
        $hash     = \Drupal::request()->request->get('hash');

        $connection  = \Drupal::database();
        $transaction = $connection->startTransaction();

        try {
            $order = OrderUtil::getOrderById($order_id);
            $real_hash = $this->getOrderHash($order);
            if ($real_hash !== $hash) throw new \Exception('invalid hash');

            $query = $connection->select('order_confirm', 'oc');
            $query->fields('oc', ['code', 'confirm_attempts']);
            $query->condition('order_id', $order_id);
            $row = $query->execute()->fetch();

            if (!$row) {
                throw new \Exception('row not found');
            }

            $attempts = intval($row->confirm_attempts);

            // Если введён неправильный код, увеличиваем количество попыток
            if ($code != $row->code) {
                $query = $connection->update('order_confirm');
                $query->fields([
                    'confirm_attempts' => $attempts + 1,
                ]);
                $query->condition('order_id', $order_id);
                $query->execute();

                // Если превысили количество попыток, отменяем заказ и удаляем строку с кодом из БД
                if ($attempts >= 4) {
                    $query = $connection->delete('order_confirm');
                    $query->condition('order_id', $order_id);
                    $query->execute();

                    $query = $connection->update('routes_users');
                    $query->fields([
                        'status' => 0,
                    ]);
                    $query->condition('id', $order_id);
                    $query->execute();

                    $response['status'] = 0;
                }

                $response['result'] = 'error';
                $response['description'] =
                    $attempts >= 4 ? 'Вы превысили количество попыток ввода кода. Ваш заказ отменён.'
                                   : 'Введён неправильный код';
            }
            // Если код верный, но пришёл невовремя, отменяем заказ и удаляем строку с кодом из БД
            // Если код верный и пришёл вовремя, подтверждаем заказ и удаляем строку с кодом из БД
            else {
                $pay_before = new \DateTime($order->pay_before);
                $now = new \DateTime();
                $in_time = $now < $pay_before;

                $query = $connection->update('routes_users');
                $query->fields([
                    'status' => $in_time ? 4 : 0,
                ]);
                $query->condition('id', $order_id);
                $query->execute();

                $query = $connection->delete('order_confirm');
                $query->condition('order_id', $order_id);
                $query->execute();

                $response['result']      = $in_time ? 'success' : 'error';
                $response['description'] = $in_time ? 'Ваш заказ подтверждён'
                                                    : 'Время для подтверждения заказа вышло. Ваш заказ отменён.';

                if (!$in_time) $response['status'] = 0;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $response['result'] = 'error';
            $response['description'] = 'Произошла ошибка. Попробуйте повторить позднее.';
        }

        return new JsonResponse($response);

    }

    public function getOrderStatus($order_id)
    {
        try {
            $order = OrderUtil::getOrderById($order_id);
            return new JsonResponse([
                'status' => $order->status,
            ]);
        } catch (\Exception $e) {
            return new JsonResponse([
                'status' => 'error',
            ]);
        }
    }
}