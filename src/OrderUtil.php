<?php
 
namespace Drupal\i1;

use \DateTime;
use \Drupal\i1\RouteUtil;

class OrderUtil {

    public static function getUserOrders($user_id)
    {
        return OrderUtil::getOrders(NULL, $user_id);
    }

    public static function getOrdersByPhoneAndDate($phone, $date)
    {
        return OrderUtil::getOrders(NULL, NULL, $phone, $date);
    }

    public static function getOrderById($order_id)
    {
        $orders = OrderUtil::getOrders($order_id, NULL, NULL, NULL);
        if (count($orders) === 0) return NULL;
        return $orders[0];
    }

    public static function getOrdersByTimetableId($timetable_id)
    {
        return OrderUtil::getOrders(NULL, NULL, NULL, NULL, $timetable_id);
    }

    private static function getOrders($order_id = NULL, $user_id = NULL, $phone = NULL, $date = NULL, $timetable_id = NULL)
    {
        $connection = \Drupal::database();
        $orders = array();

        $query = $connection->select('routes_users', 'ru');
        $query->fields('ru', array(
            'id',
            'user_id',
            'price',
            'routetimetable_id',
            'status',
            'from_id',
            'to_id',
            'personal_data',
            'payment_id',
            'sms_sent',
            'pay_before',
        ));
        if ($order_id) $query->condition('id', $order_id);
        if ($user_id)  $query->condition('user_id', $user_id);
        if ($phone)    $query->condition('ru.personal_data', '%'.$phone.'%', 'LIKE');
        if ($timetable_id) $query->condition('routetimetable_id', $timetable_id);
        $result = $query->execute()->fetchAll();
        
        foreach ($result as $row) {
            $order = $row;

            $order->id      = intval($order->id);
            $order->user_id = intval($order->user_id);
            $order->status  = intval($order->status);

            $query = $connection->select('routetimetable', 'rt');
            $query->addField('rt', 'route_pattern_id');
            $query->condition('rt.id', $row->routetimetable_id);
            $route_pattern_id = $query->execute()->fetchField();

            $node = entity_load('node', $route_pattern_id);
            $order->route_name = $node->get('field_nazvanie_dla_passazirov')->value;
            $order->route_type = $node->getType();
            
            $order->route = RouteUtil::getSubRoute($row->routetimetable_id, $row->from_id, $row->to_id);
            $order->passengers = array();
            $order->phones = explode("\n", $row->personal_data);

            if (!$date || $date === $order->route[0]->arrival_date_time->format('Y-m-d')) {
                $orders[] = $order;
            }
        }

        return $orders;
    }
}