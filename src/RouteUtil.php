<?php
 
namespace Drupal\i1;

use DateInterval;
use \DateTime;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Database\Query\Condition;

class RouteUtil {

    public static function getRouteTimetableById($id, $future = TRUE)
    {
        $connection = \Drupal::database();
        $query = $connection->select('routetimetable', 'rt');
        $query->condition('route_date_time', date_format(new DateTime('today'), 'Y-m-d H:i:s'), $future ? '>' : '<');
        $query->condition('rt.route_pattern_id', $id);
        $query->fields('rt', array(
            'id',
            'route_date_time',
            'tickets_sold',
            'status',
        ));
        $query->orderBy('rt.route_date_time', 'DESC');
        $timetable = $query->execute()->fetchAll();
        $unique_dates = RouteUtil::getRouteTimetableDatesById($id, $future);
        rsort($unique_dates);

        $result = array();
        foreach ($unique_dates as $date) { // Для каждой даты
            $formatted_date = RouteUtil::formatDate(DateTime::createFromFormat('Y-m-d', $date));
            $result[$formatted_date] = array();
            foreach ($timetable as $value) {
                if (substr($value->route_date_time, 0, -9) === $date) { // Для каждого времени, прописанного вместе с этой датой
                    $result[$formatted_date][] = new \stdClass();
                    $result[$formatted_date][count($result[$formatted_date]) - 1]->id           = $value->id;
                    $result[$formatted_date][count($result[$formatted_date]) - 1]->time         = substr($value->route_date_time, -8, 5);
                    $result[$formatted_date][count($result[$formatted_date]) - 1]->tickets_sold = $value->tickets_sold;
                    $result[$formatted_date][count($result[$formatted_date]) - 1]->status       = $value->status;
                }
            }
        }
        return $result;
    }

    public static function getRouteTimetableDatesById($id, $future = TRUE)
    {
        $connection = \Drupal::database();
        $query = $connection->select('routetimetable', 'rt');
        $query->condition('route_date_time', date_format(new DateTime('today'), 'Y-m-d H:i:s'), $future ? '>' : '<');
        $query->condition('rt.route_pattern_id', $id);
        $query->fields('rt', array(
            'route_date_time'
        ));
        $result =  $query->execute()->fetchAll();

        $func = function($value) {
            return substr($value->route_date_time, 0, -9);
        };

        return array_values(array_unique(array_map($func, $result)));
    }

    public static function formatDate(DateTime $date) {
        $months = array(
            '01' => 'января',
            '02' => 'февраля',
            '03' => 'марта',
            '04' => 'апреля',
            '05' => 'мая',
            '06' => 'июня',
            '07' => 'июля',
            '08' => 'августа',
            '09' => 'сентября',
            '10' => 'октября',
            '11' => 'ноября',
            '12' => 'декабря'
        );

        return $date->format('j ').$months[$date->format('m')];
    }

    public static function formatDateForSmsOrderInfo(DateTime $date) {
        $months = array(
            '01' => 'янв',
            '02' => 'фев',
            '03' => 'мар',
            '04' => 'апр',
            '05' => 'мая',
            '06' => 'июн',
            '07' => 'июл',
            '08' => 'авг',
            '09' => 'сен',
            '10' => 'окт',
            '11' => 'ноя',
            '12' => 'дек'
        );

        return $date->format('j ').$months[$date->format('m')].$date->format(' H:i');
    }

    public static function getCitiesList($node_type) {
        $taxonomy_vocabularies_mapping = array(
            'mezdugorodnii_reis' => 'naselennye_punkty',
            'ekskursia' => 'punkty_ekskursii',
            'tur' => 'punkty_turov',
        );

        $cities_list = array();

        if ($node_type === 'all') {
            foreach ($taxonomy_vocabularies_mapping as $key => $value) {
                $current_cities_list = RouteUtil::getCitiesList($key);
                $cities_list = $cities_list + $current_cities_list;
            }
            return $cities_list;
        }

        $cities = \Drupal::entityTypeManager()->getStorage('taxonomy_term')
                                              ->loadTree($taxonomy_vocabularies_mapping[$node_type]);
        foreach ($cities as $city) {
            $ancestors = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadAllParents($city->tid);
            $list = [];
            foreach ($ancestors as $a) {
              $list[] = $a->id();
            }
            $parent_id = count($list) > 1 ? intval($list[1]) : 0;
            $cities_list[$city->tid] = (object) array(
                "name" => $city->name,
                "parent_id" => $parent_id,
            );
            $cities_list[-1] = (object) array(
                "name" => "",
                "parent_id" => "-1",
            );
        }
        return $cities_list;
    }

    public static function getSubRoute($id, $from, $to) {
        $connection = \Drupal::database();
        $query = $connection->select('routetimetable', 'rt');
        $query->condition('rt.id', $id);
        $query->addField('rt', 'route_date_time');
        $query->addField('rt', 'route_pattern_id');
        $route = $query->execute()->fetchAll()[0];

        $query = $connection->select('routepattern_cities', 'rpc');
        $query->fields('rpc', array('city_id', 'station_id', 'time_after_prev', 'stop_time'));
        $query->condition('rpc.route_pattern_id', $route->route_pattern_id);
        $query->orderBy('rpc.city_order');
        $cities = $query->execute()->fetchAll();

        $start_date_time = new DateTime($route->route_date_time);
        $cities[0]->arrival_date_time = clone $start_date_time;
        $cities[0]->departure_date_time = clone $start_date_time;
        
        for ($i = 1; $i < count($cities); $i++) {
            $previous_city_id = $cities[$i - 1]->city_id;
            $current_city_id  = $cities[$i]->city_id;
            // Рассчитываем время прибытия
            $cities[$i]->arrival_date_time = clone $cities[$i-1]->departure_date_time;
            $minutes = intval($cities[$i]->time_after_prev);
            date_add($cities[$i]->arrival_date_time, date_interval_create_from_date_string($minutes.' minutes'));
            RouteUtil::changeDateTimeZone($cities[$i]->arrival_date_time, $previous_city_id, $current_city_id);
            // Рассчитываем время отправления
            $cities[$i]->departure_date_time = clone $cities[$i]->arrival_date_time;
            $minutes = intval($cities[$i]->stop_time);
            date_add($cities[$i]->departure_date_time, date_interval_create_from_date_string($minutes.' minutes'));
        }

        $new = array();
        list($from_index, $to_index) = RouteUtil::getBorderIndices($cities, $from, $to);
        foreach ($cities as $k => $v) {
            if ($k >= $from_index && $k <= $to_index) $new[] = clone $v;
        }

        $new[0]->arrival_date_time = $new[0]->departure_date_time; // Симуляция начального пункта маршрута
        $new[0]->stop_time = 0; // Симуляция начального пункта маршрута

        $cities_list = RouteUtil::getCitiesList('all');

        foreach ($new as $r) {
            $r->city_name      = isset($cities_list[$r->city_id]) ? $cities_list[$r->city_id]->name : 'Город, которого нет';
            $station_id = $r->station_id ? $r->station_id : -1;
            $r->station_name   = isset($cities_list[$station_id]) ? $cities_list[$station_id]->name : 'Вокзал, которого нет';
            $r->coords         = RouteUtil::getCoords($r->station_id ? $r->station_id : $r->city_id);
            $r->arrival_date   = RouteUtil::formatDate($r->arrival_date_time);
            $r->departure_date = RouteUtil::formatDate($r->departure_date_time);
            $r->arrival_time   = $r->arrival_date_time->format('H:i');
            $r->departure_time = $r->departure_date_time->format('H:i');

            $days      = (int)floor($r->stop_time / 1440);
            $remainder = $r->stop_time % 1440;
            $hours     = (int)floor($remainder / 60);
            $minutes   = $remainder % 60;

            $r->stop_time_days    = $days;
            $r->stop_time_hours   = $hours;
            $r->stop_time_minutes = $minutes;
        }

        return $new;
    }

    public static function getSubRoutePrice($route_pattern_id, $from, $to) {
        $connection = \Drupal::database();
        $query = $connection->select('routepattern_cities', 'rpc');
        $query->fields('rpc', array('price_after_prev', 'city_id', 'station_id'));
        $query->condition('rpc.route_pattern_id', $route_pattern_id);
        $query->orderBy('rpc.city_order');
        $cities = $query->execute()->fetchAll();

        $price = 0;

        list($from_index, $to_index) = RouteUtil::getBorderIndices($cities, $from, $to);
        foreach ($cities as $k => $v) {
            if ($k >= $from_index + 1 && $k <= $to_index) $price += floatval($v->price_after_prev);
        }

        return $price;
    }

    private static function getBorderIndices($route, $from, $to) {
        if ($from === '0' && $to === '0') {
            return array(0, count($route) - 1);
        }
        
        $from_index = -1;
        $to_index = -1;

        for ($i = 0; $i < count($route); $i++) { 
            if ($from === $route[$i]->city_id || $from === $route[$i]->station_id) $from_index = $from_index === -1 ? $i : $from_index;
            if ($to === $route[$i]->city_id   || $to === $route[$i]->station_id)   $to_index = $i;
        }

        if ($from_index === -1) $from_index = 0;
        if ($to_index === -1)   $to_index = count($route) - 1;

        return array($from_index, $to_index);
    }

    private static function getCoords($term_id)
    {
        $term = taxonomy_term_load($term_id);
        if ($term && $term->hasField('field_koordinaty')) {
            if (!$term->get('field_koordinaty')->isEmpty()) {
                $coords = $term->get('field_koordinaty')->getValue();
                return array(
                    'latitude' => $coords[0]['value'],
                    'longitude' => $coords[1]['value'],
                );
            }
        }
        return false;
    }

    public static function changeDateTimeZone(DateTime &$datetime, $start_city_id, $needed_city_id) {
        $connection = \Drupal::database();
        $query = $connection->select('taxonomy_term__field_timezone', 'tz');
        $query->addField('tz', 'entity_id');
        $query->addField('tz', 'field_timezone_value', 'timezone');

        $city_condition = new Condition('OR');
        $city_condition->condition('entity_id', $start_city_id);
        $city_condition->condition('entity_id', $needed_city_id);
        $query->condition($city_condition);

        $result = $query->execute()->fetchAllKeyed();
        $start_city_time_offset =
            isset($result[intval($start_city_id)])  ? intval($result[intval($start_city_id)])  : 2;
        $needed_city_time_offset =
            isset($result[intval($needed_city_id)]) ? intval($result[intval($needed_city_id)]) : 2;
        $time_diff = $needed_city_time_offset - $start_city_time_offset;

        if ($time_diff >= 0) {
            $datetime->add(new DateInterval('PT'.$time_diff.'H'));
        } else {
            $datetime->sub(new DateInterval('PT'.abs($time_diff).'H'));
        }
    }
}