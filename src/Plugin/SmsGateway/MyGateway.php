<?php

namespace Drupal\i1\Plugin\SmsGateway;

use Drupal\i1\Sms\SmsSender;
use Drupal\sms\Direction;
use Drupal\sms\Message\SmsMessageInterface;
use Drupal\sms\Message\SmsDeliveryReport;
use Drupal\sms\Message\SmsMessageReportStatus;
use Drupal\sms\Message\SmsMessageResult;
use Drupal\sms\Plugin\SmsGatewayPluginBase;

/**
 * @SmsGateway(
 *   id = "my_gateway",
 *   label = "My Gateway",
 *   outgoing_message_max_recipients = 1,
 * )
 */
class MyGateway extends SmsGatewayPluginBase
{

    /**
     * Sends an SMS.
     *
     * @param \Drupal\sms\Message\SmsMessageInterface $sms
     *   The sms to be sent.
     *
     * @return \Drupal\sms\Message\SmsMessageResultInterface
     *   The result of the sms messaging operation.
     */
    public function send(SmsMessageInterface $sms)
    {
        $direction = $sms->getDirection();

        $message = $sms->getMessage();
        $number = $sms->getRecipients()[0];

        if ($direction === Direction::OUTGOING) {
            $sender = new SmsSender();
            $sender->sendSms($number, $message);
        }

        // Последующий код скопирован из примеров
        $result = new SmsMessageResult();
        $report = new SmsDeliveryReport();
        $report->setRecipient($number);
        $report->setStatus(SmsMessageReportStatus::QUEUED);
        $report->setMessageId(time());

        if ($report->getStatus()) {
            $result->addReport($report);
        }

        return $result;

    }
}