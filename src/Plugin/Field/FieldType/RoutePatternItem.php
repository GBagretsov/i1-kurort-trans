<?php
 
namespace Drupal\i1\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of RoutePattern.
 * 
 * @FieldType(
 *   id = "route_pattern",
 *   label = @Translation("Шаблон маршрута"),
 *   module = "i1",
 *   default_formatter = "route_pattern_default_formatter",
 *   default_widget = "route_pattern_default_widget",
 * )
 */

class RoutePatternItem extends FieldItemBase implements FieldItemInterface {
    public static function schema(FieldStorageDefinitionInterface $field_definition) {
        $columns = [];

        $columns['node_id'] = [
          'type' => 'int',
          'length' => 11,
        ];

        return [
          'columns' => $columns,
          'indexes' => [],
        ];
    }

    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
        $properties = [];
        $properties['node_id'] = DataDefinition::create('integer');

        return $properties;
    }

    public function isEmpty() {
        return false;
    }
}