<?php
 
namespace Drupal\i1\Plugin\Field\FieldWidget;

use \DateTime;
use \Drupal\i1\RouteUtil;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * A widget for Route pattern.
 *
 * @FieldWidget(
 *   id = "route_pattern_default_widget",
 *   module = "i1",
 *   label = @Translation("Форма редактирования шаблона маршрута"),
 *   field_types = {
 *     "route_pattern"
 *   }
 * )
 */
class RoutePatternDefaultWidget extends WidgetBase {
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
        
        $current_path_parts = explode("/", \Drupal::service('path.current')->getPath());
        $node = entity_load('node', $items[$delta]->node_id);
        $node_type = is_null($node) ? end($current_path_parts) : $node->bundle();

        $element['node_type'] = [
            '#type' => 'hidden',
            '#title' => t('Тип материала'),
            '#value' => $node_type,
        ];

        $element['cities_list'] = [
            '#type' => 'hidden',
            '#title' => t('Список городов'),
            '#value' => json_encode(RouteUtil::getCitiesList($node_type)),
        ];

        $route_pattern = isset($items[$delta]->node_id) ? $this->getRoutePatternById($items[$delta]->node_id) : NULL;
        $route_info    = isset($items[$delta]->node_id) ? $this->getRouteInfoById($items[$delta]->node_id)    : NULL;
        
        $sale_opened       = isset($items[$delta]->node_id) ? RouteUtil::getRouteTimetableById($items[$delta]->node_id) : array();
        $sale_opened_dates = isset($items[$delta]->node_id) ? RouteUtil::getRouteTimetableDatesById($items[$delta]->node_id) : array();

        $element['sale_opened'] = [
            '#theme' => 'sale_opened_form',
            '#title' => t('Рейсы в продаже'),
            '#result' => $sale_opened,
            '#node_id' => $items[$delta]->node_id,
        ];

        $element['sale_opened_dates'] = [
            '#type' => 'hidden',
            '#title' => t('Рейсы в продаже'),
            '#value' => json_encode($sale_opened_dates),
        ];

        $element['date_time_rules'] = array(
            '#type' => 'details',
            '#title' => t('Дата и время'),
            '#open' => TRUE,
        );

        $element['date_time_rules']['date_rules'] = [
            '#type' => 'textarea',
            '#title' => t('Выбор дат'),
            '#default_value' => isset($route_pattern->date_rules) ? $route_pattern->date_rules : '',
            '#description' => count($sale_opened) > 0 ? t('Доступен предпросмотр выбранных дат на три месяца<br/>Красным выделены даты, для которых открыта продажа &mdash; их отключить нельзя')
                                                      : t('Доступен предпросмотр выбранных дат на три месяца'),
        ];

        $element['date_time_rules']['time_rules'] = [
            '#type' => 'textarea',
            '#title' => t('Выбор времени'),
            '#default_value' => isset($route_pattern->time_rules) ? $route_pattern->time_rules : '',
            '#description' => count($sale_opened) > 0 ? t('У тех рейсов, на которые уже есть проданные билеты, время не изменится')
                                                      : '',
        ];
        
        $element['route'] = array(
            '#type' => 'details',
            '#title' => t('Маршрут'),
            '#open' => TRUE,
        );

        $element['route']['route'] = [
            '#type' => 'textarea',
            '#title' => t('Маршрут'),
            '#default_value' => isset($route_info) ? $route_info : '',
            '#description' => t('Вы можете сортировать пункты маршрута, перемещая их порядковые номера'),
        ];

        $element['node_id'] = [
            '#type' => 'hidden',
            '#default_value' => isset($items[$delta]->node_id) ? $items[$delta]->node_id : 0,
        ];

        return $element;
    }

    private function getRoutePatternById($id)
    {
        $connection = \Drupal::database();
        $query = $connection->select('routepattern', 'rp');
        $query->condition('rp.id', $id);
        $query->fields('rp', array(
            'price', 
            'tickets_amount', 
            'open_sale_before', 
            'date_rules', 
            'time_rules'
        ));
        return $query->execute()->fetchObject();
    }

    private function getRouteInfoById($id)
    {
        $connection = \Drupal::database();
        $query = $connection->select('routepattern_cities', 'rpc');
        $query->condition('rpc.route_pattern_id', $id);
        $query->fields('rpc', array(
            'city_id',
            'station_id',
            'time_after_prev',
            'distance_after_prev',
            'price_after_prev',
            'stop_time'
        ));
        $query->orderBy('rpc.city_order');
        $result =  $query->execute()->fetchAll();

        $route_output = "";

        foreach ($result as $r) {
            $route_output .= $r->city_id." ";
            $route_output .= isset($r->station_id) ? $r->station_id." " : "-1 ";
            $route_output .= $r->time_after_prev." ";
            $route_output .= $r->distance_after_prev." ";
            $route_output .= $r->stop_time." ";
            $route_output .= $r->price_after_prev."\n";

        }

        return trim($route_output);
    }

}
