<?php
 
namespace Drupal\i1\Plugin\Field\FieldFormatter;

use Drupal\i1\RouteUtil;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * A formatter for Route pattern.
 *
 * @FieldFormatter(
 *   id = "route_pattern_default_formatter",
 *   module = "i1",
 *   label = @Translation("Отображение шаблона маршрута"),
 *   field_types = {
 *     "route_pattern"
 *   }
 * )
 */

class RoutePatternDefaultFormatter extends FormatterBase {
    public function viewElements(FieldItemListInterface $items, $langcode) {
        $element = [];
        $connection = \Drupal::database();

        foreach ($items as $delta => $item) {
            $id = $item->node_id;

            $node = entity_load('node', $id);
            $node_type = $node->bundle();
            
            $query = $connection->select('routepattern', 'rp');
            $query->condition('rp.id', $id);
            $query->addField('rp', 'id');
            $query->addField('rp', 'open_sale_before');
            $query->addField('rp', 'price');
            $route_pattern = $query->execute()->fetchAll()[0];

            $node = entity_load('node', $route_pattern->id);
            $route_type = $node->getType();

            $query = $connection->select('routetimetable', 'rt');
            $query->condition('rt.route_pattern_id', $id);
            $query->condition('status', 1);
            $query->condition('tickets_sold', intval($node->field_kolicestvo_biletov->value), '<');
            $query->addField('rt', 'id');
            $query->addField('rt', 'route_date_time');
            $query->addField('rt', 'status');
            $timetable = $query->execute()->fetchAll();

            $query = $connection->select('routepattern_cities', 'rpc');
            $query->addField('rpc', 'city_id');
            $query->addField('rpc', 'station_id');
            $query->addField('rpc', 'time_after_prev');
            $query->addField('rpc', 'distance_after_prev');
            $query->addField('rpc', 'stop_time');
            $query->condition('rpc.route_pattern_id', $id);
            $query->orderBy('rpc.city_order');
            $route = $query->execute()->fetchAll();

            $cities_list = RouteUtil::getCitiesList($node_type);
            
            foreach ($route as $r) {
                $r->city_name      = isset($cities_list[$r->city_id]) ? $cities_list[$r->city_id]->name : 'Город, которого нет';
                $station_id = $r->station_id ? $r->station_id : -1;
                $r->station_name   = isset($cities_list[$station_id]) ? $cities_list[$station_id]->name : 'Вокзал, которого нет';

                $days      = (int)floor($r->time_after_prev / 1440);
                $remainder = $r->time_after_prev % 1440;
                $hours     = (int)floor($remainder / 60);
                $minutes   = $remainder % 60;

                $r->time_after_prev_days    = $days;
                $r->time_after_prev_hours   = $hours;
                $r->time_after_prev_minutes = $minutes;

                $days      = (int)floor($r->stop_time / 1440);
                $remainder = $r->stop_time % 1440;
                $hours     = (int)floor($remainder / 60);
                $minutes   = $remainder % 60;

                $r->stop_time_days    = $days;
                $r->stop_time_hours   = $hours;
                $r->stop_time_minutes = $minutes;
            }

            // TODO: сортировать по годам и месяцам?
            $route_pattern_history = array();
            if (\Drupal::currentUser()->hasPermission('edit content')) {
                $route_pattern_history = RouteUtil::getRouteTimetableById($id, FALSE);
            }

            $element[$delta] = [
                '#type' => 'markup',
                '#theme' => 'route_pattern',
                '#route_pattern' => $route_pattern,
                '#timetable' => $timetable,
                '#route' => $route,
                '#route_type' => $route_type,
                '#route_pattern_history' => $route_pattern_history,
                '#cache' => ['max-age' => 0,],
            ];
        }

        \Drupal::service('page_cache_kill_switch')->trigger();

        return $element;
    }
}